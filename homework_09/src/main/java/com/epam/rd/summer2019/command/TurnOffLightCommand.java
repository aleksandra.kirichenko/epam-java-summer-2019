package com.epam.rd.summer2019.command;

public class TurnOffLightCommand implements Command {
    private Light light;

    TurnOffLightCommand(Light light){
        this.light =light;
    }

    public void execute(){
        light.turnOff();
    }
}
