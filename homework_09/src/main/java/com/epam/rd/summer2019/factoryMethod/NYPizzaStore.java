package com.epam.rd.summer2019.factoryMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NYPizzaStore extends PizzaStore {
    private static Logger logger = LoggerFactory.getLogger(NYPizzaStore.class.getName());

    public Pizza createPizza(PizzaType type) {
        switch (type) {
            case CHEESE:
                return new NYStyleCheesePizza();
            case VEGGIE:
                return new NYStyleVeggiePizza();
            default:
                String message = String.format("Unknown pizza type: %s", type);
                logger.error(message);
                throw new IllegalArgumentException(message);

        }
    }
}

