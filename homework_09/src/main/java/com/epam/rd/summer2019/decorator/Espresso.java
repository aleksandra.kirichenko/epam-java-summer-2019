package com.epam.rd.summer2019.decorator;

public class Espresso extends Beverage {
    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 21;
    }
}
