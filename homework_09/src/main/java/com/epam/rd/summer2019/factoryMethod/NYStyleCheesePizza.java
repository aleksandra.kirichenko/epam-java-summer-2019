package com.epam.rd.summer2019.factoryMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NYStyleCheesePizza extends Pizza {
    private static Logger logger = LoggerFactory.getLogger(NYStyleCheesePizza.class.getName());

    public NYStyleCheesePizza() {
        name = "NYStyleCheese Pizza";
        dough = "Thick";
        sauce = "Extra Cheese Pizza Sauce";
    }

    String bake() {
        String m = "Bake for 45 minutes";
        logger.info(m);
        return m;
    }

}
