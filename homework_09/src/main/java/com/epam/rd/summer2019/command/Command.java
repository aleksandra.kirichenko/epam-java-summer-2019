package com.epam.rd.summer2019.command;

public interface Command {
        void execute();
}
