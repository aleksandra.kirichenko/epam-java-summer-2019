package com.epam.rd.summer2019.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelDuck extends Duck {
    private static Logger logger = LoggerFactory.getLogger(ModelDuck.class.getName());

    ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new MuteQuack();
    }

    @Override
    public String display() {
        String m = "I am model Duck";
        logger.info(m);
        return "I am model Duck";
    }
}
