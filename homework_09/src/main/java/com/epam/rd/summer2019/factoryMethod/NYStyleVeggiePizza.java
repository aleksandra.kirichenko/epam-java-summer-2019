package com.epam.rd.summer2019.factoryMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NYStyleVeggiePizza extends Pizza {
    private static Logger logger = LoggerFactory.getLogger(NYStyleVeggiePizza.class.getName());

    public NYStyleVeggiePizza() {
        name = "NYStyleVeggie Pizza";
        dough = "Extra Thick Crust";
        sauce = "Marinara sauce";
    }
    String bake(){
        String m = "Bake for 40 minutes";
        logger.info(m);
        return m;
    }
}
