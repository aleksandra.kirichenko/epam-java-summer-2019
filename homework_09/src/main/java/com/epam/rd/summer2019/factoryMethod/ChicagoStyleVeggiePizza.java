package com.epam.rd.summer2019.factoryMethod;

public class ChicagoStyleVeggiePizza extends Pizza {
    public ChicagoStyleVeggiePizza() {
        name = "ChicagoStyleVeggie Pizza";
        dough = "Thin Crust";
        sauce = "Marinara sauce";
    }
}
