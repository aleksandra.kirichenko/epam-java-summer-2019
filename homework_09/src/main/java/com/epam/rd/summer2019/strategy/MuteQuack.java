package com.epam.rd.summer2019.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MuteQuack implements QuackBehavior {
    private static Logger logger = LoggerFactory.getLogger(MuteQuack.class.getName());

    @Override
    public String quack() {
        String m = "<<Silence>>";
        logger.info(m);
        return m;
    }
}
