package com.epam.rd.summer2019.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Light {
    private static Logger logger = LoggerFactory.getLogger(Light.class.getName());

    protected void turnOn(){
        logger.info("The light is on");
    }

    protected void turnOff(){
        logger.info("The light is off");
    }
}
