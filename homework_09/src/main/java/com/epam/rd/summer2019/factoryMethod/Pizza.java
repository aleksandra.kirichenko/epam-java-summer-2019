package com.epam.rd.summer2019.factoryMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Pizza {
    private static Logger logger = LoggerFactory.getLogger(Pizza.class.getName());

    String name;
    String dough;
    String sauce;

    String prepare() {

        String m = "Preparing";
        logger.info(m);
        return m;
    }

    String bake() {
        String m = "Bake for 25 minutes";
        logger.info(m);
        return m;
    }

    String cut() {
        String m = "Cutting the pizza into diagonal slices";
        logger.info(m);
        return m;
    }

    String box() {
        String m = "Boxing";
        logger.info(m);
        return m;
    }

    public String getName() {
        return name;
    }

    public String getDough() {
        return dough;
    }

    public String getSauce() {
        return sauce;
    }
}
