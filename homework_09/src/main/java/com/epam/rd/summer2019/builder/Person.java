package com.epam.rd.summer2019.builder;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String fathersName;
    private String mothersName;
    private double height;

    public Person(String firstName, String lastName, int age, String fathersName, String mothersName, double height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.fathersName = fathersName;
        this.mothersName = mothersName;
        this.height = height;
    }
    public static class Builder {
        private String firstName;
        private String lastName;
        private int age;
        private String fathersName;
        private String mothersName;
        private double height;

        public Builder() {
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setFathersName(String fathersName) {
            this.fathersName = fathersName;
            return this;
        }

        public Builder setMothersName(String mothersName) {
            this.mothersName = mothersName;
            return this;
        }

        public Builder setHeight(double height) {
            this.height = height;
            return this;
        }

        public Person build() {
            return new Person(firstName, lastName, age, fathersName, mothersName, height);
        }

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getMothersName() {
        return mothersName;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", fathersName='" + fathersName + '\'' +
                ", mothersName='" + mothersName + '\'' +
                ", height=" + height +
                '}';
    }
}
