package com.epam.rd.summer2019.decorator;

public class DarkRoast extends Beverage {
    public DarkRoast(){
        description = "Dark Roast";
    }

    @Override
    public double cost() {
        return 18;
    }
}
