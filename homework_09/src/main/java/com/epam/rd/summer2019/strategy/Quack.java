package com.epam.rd.summer2019.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Quack implements QuackBehavior {
    private static Logger logger = LoggerFactory.getLogger(Quack.class.getName());

    @Override
    public String quack(){
        String m = "Quack";
        logger.info("Quack");
        return m;
    }
}
