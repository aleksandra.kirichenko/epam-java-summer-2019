package com.epam.rd.summer2019.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MallardDuck extends Duck {
    private static Logger logger = LoggerFactory.getLogger(MallardDuck.class.getName());

    MallardDuck(){
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    public String display(){
        String m = "I am real mallard duck";
        logger.info(m);
        return m;
    }
}
