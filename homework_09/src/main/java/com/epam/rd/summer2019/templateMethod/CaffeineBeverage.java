package com.epam.rd.summer2019.templateMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CaffeineBeverage {
    private static Logger logger = LoggerFactory.getLogger(CaffeineBeverage.class.getName());

    final String prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        addCondiments();
        return "" + boilWater() + brew() + pourInCup() + addCondiments();
    }

    abstract String brew();

    abstract String addCondiments();

    String boilWater() {

        String message = "Boiling water";
        logger.info(message);
        return message;
    }

    String pourInCup() {
        String message = "Pouring into cup";
        logger.info(message);
        return message;
    }
}
