package com.epam.rd.summer2019.factoryMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChicagoStyleCheesePizza extends Pizza {
    private static Logger logger = LoggerFactory.getLogger(ChicagoStyleCheesePizza.class.getName());

    public ChicagoStyleCheesePizza() {
        name = "ChicagoStyleCheese Pizza";
        dough = "Regular Crust";
        sauce = "Plum Tomato Sauce";
    }
    String cut(){
        String m = "Cutting the pizza into square slices";
        logger.info(m);
        return m;
    }
}
