package com.epam.rd.summer2019.strategy;

public abstract class Duck {
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    Duck() {
    }

    public abstract String display();

    String performFly() {
        return flyBehavior.fly();
    }

    String performQuack() {
        return quackBehavior.quack();
    }
}
