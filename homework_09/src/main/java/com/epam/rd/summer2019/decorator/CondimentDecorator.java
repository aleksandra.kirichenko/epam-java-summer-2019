package com.epam.rd.summer2019.decorator;

public abstract class CondimentDecorator extends Beverage {
    public abstract String getDescription();

}
