package com.epam.rd.summer2019.factoryMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChicagoPizzaStore extends PizzaStore {
    private static Logger logger = LoggerFactory.getLogger(ChicagoPizzaStore.class.getName());

    public Pizza createPizza(PizzaType type) {
        switch (type) {
            case CHEESE:
                return new ChicagoStyleCheesePizza();
            case VEGGIE:
                return new ChicagoStyleVeggiePizza();
            default:
                String message = String.format("Unknown pizza type: %s", type);
                logger.error(message);
                throw new IllegalArgumentException(message);
        }
    }
}