package com.epam.rd.summer2019.strategy;

public interface FlyBehavior {
    String fly();
}
