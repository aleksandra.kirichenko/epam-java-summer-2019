package com.epam.rd.summer2019.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlyWithWings implements FlyBehavior {
    private static Logger logger = LoggerFactory.getLogger(FlyWithWings.class.getName());

    @Override
    public String fly() {
        String m = "I am flying!!";
        logger.info(m);
        return m;
    }
}
