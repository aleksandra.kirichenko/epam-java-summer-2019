package com.epam.rd.summer2019.strategy;

public interface QuackBehavior {
    public String quack();
}
