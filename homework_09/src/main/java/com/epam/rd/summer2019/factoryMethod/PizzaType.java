package com.epam.rd.summer2019.factoryMethod;

public enum PizzaType {
    VEGGIE, CHEESE, TOMATO
}
