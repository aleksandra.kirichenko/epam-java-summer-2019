package com.epam.rd.summer2019.command;

public class TurnOnLightCommand implements Command {
    Light light;

    TurnOnLightCommand(Light light){
        this.light =light;
    }

    public void execute(){
        this.light.turnOn();
    }
}
