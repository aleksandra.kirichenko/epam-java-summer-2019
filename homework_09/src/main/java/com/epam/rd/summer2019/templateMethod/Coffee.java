package com.epam.rd.summer2019.templateMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Coffee extends CaffeineBeverage {
    private static Logger logger = LoggerFactory.getLogger(Coffee.class.getName());
    @Override
    String brew() {
        String message = "Dripping coffee through filter";
        logger.info(message);
        return message;
    }

    @Override
    String addCondiments() {
        String message = "Adding Sugar and Milk";
        logger.info(message);
        return message;
    }
}
