package com.epam.rd.summer2019.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlyNoWay implements FlyBehavior {

    private static Logger logger = LoggerFactory.getLogger(FlyNoWay.class.getName());

    @Override
    public String fly() {
        String m = "I can't fly";
        logger.info(m);
        return m;
    }
}
