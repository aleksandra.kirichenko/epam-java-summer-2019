package com.epam.rd.summer2019.templateMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tea extends CaffeineBeverage {
    private static Logger logger = LoggerFactory.getLogger(Tea.class.getName());
    @Override
    String brew() {
        String message = "Steeping the tea";
        logger.info(message);
        return message;
    }
    @Override
    String addCondiments() {
        String message= "Adding Lemon";
        logger.info(message);
        return message;
    }
}
