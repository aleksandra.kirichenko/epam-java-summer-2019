package com.epam.rd.summer2019.decorator;

public class Decaf extends Beverage {
    public Decaf(){
        description ="Decaf";
    }

    @Override
    public double cost() {
        return 1.05;
    }
}
