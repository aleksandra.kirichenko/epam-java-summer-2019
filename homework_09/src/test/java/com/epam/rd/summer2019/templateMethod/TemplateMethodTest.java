package com.epam.rd.summer2019.templateMethod;

import org.junit.Assert;
import org.junit.Test;

public class TemplateMethodTest {
    @Test
    public void recipe() {
        // GIVEN
        CaffeineBeverage tea = new Tea();
        CaffeineBeverage coffee = new Coffee();
        // WHEN  prepare recipe
        // THEN
        Assert.assertEquals("Boiling water" + "Steeping the tea" + "Pouring into cup" + "Adding Lemon", tea.prepareRecipe());
        Assert.assertEquals("Boiling water" + "Dripping coffee through filter" + "Pouring into cup" + "Adding Sugar and Milk", coffee.prepareRecipe());
    }

}