package com.epam.rd.summer2019.decorator;

import org.junit.Assert;
import org.junit.Test;

public class DecoratorTest {

    @Test
    public void description() {
        // GIVEN
        Beverage espresso = new Espresso();
        Beverage darkRoast = new Soy(new DarkRoast());
        Beverage decaf = new Whip(new Decaf());
        Beverage houseBlend = new Mocha(new Mocha(new HouseBlend()));

        // WHEN get description
        // THEN
        Assert.assertEquals("Espresso", espresso.getDescription());
        Assert.assertEquals("Dark Roast, Soy", darkRoast.getDescription());
        Assert.assertEquals("Decaf, Whip", decaf.getDescription());
        Assert.assertEquals("House Blend Coffee, Mocha, Mocha", houseBlend.getDescription());
    }

    @Test
    public void cost() {
        // GIVEN
        Beverage darkRoast = new Mocha(new Mocha(new Whip((new DarkRoast()))));
        Beverage espresso = new Mocha(new Espresso());
        Beverage houseBlend = new Soy(new HouseBlend());

        // WHEN cost
        // THEN
        Assert.assertEquals(18.55, darkRoast.cost(), .05);
        Assert.assertEquals(21.20, espresso.cost(), .05);
        Assert.assertEquals(11.15, houseBlend.cost(), .05);

    }

}