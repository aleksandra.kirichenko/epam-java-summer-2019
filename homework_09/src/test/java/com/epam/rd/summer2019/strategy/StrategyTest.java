package com.epam.rd.summer2019.strategy;

import org.junit.Assert;
import org.junit.Test;

public class StrategyTest {

    @Test
    public void display() {
        // GIVEN
        Duck modelDuck = new ModelDuck();
        Duck mallardDuck = new MallardDuck();
        // WHEN display
        // THEN
        Assert.assertEquals("I am model Duck", modelDuck.display());
        Assert.assertEquals("I am real mallard duck", mallardDuck.display());
    }

    @Test
    public void flyWithWings() {
        // GIVEN
        Duck mallardDuck = new MallardDuck();
        // WHEN perform fly
        // THEN
        Assert.assertEquals("I am flying!!", mallardDuck.performFly());
    }

    @Test
    public void flyNoWay() {
        // GIVEN
        Duck modelDuck = new ModelDuck();
        // WHEN perform fly
        // THEN
        Assert.assertEquals("I can't fly", modelDuck.performFly());

    }

    @Test
    public void muteQuack() {
        // GIVEN
        Duck modelDuck = new ModelDuck();
        // WHEN perform quack
        // THEN
        Assert.assertEquals("<<Silence>>", modelDuck.performQuack());

    }

    @Test
    public void quack() {
        // GIVEN
        Duck mallardDuck = new MallardDuck();
        // WHEN quack
        // THEN
        Assert.assertEquals("Quack", mallardDuck.performQuack());
    }


}