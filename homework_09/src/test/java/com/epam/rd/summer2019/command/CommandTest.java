package com.epam.rd.summer2019.command;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class CommandTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    Light light;
    @InjectMocks
    TurnOnLightCommand flipUpCommand = new TurnOnLightCommand(light);
    @InjectMocks
    TurnOffLightCommand flipDownCommand = new TurnOffLightCommand(light);

    private Switch lightSwitcher = new Switch(flipUpCommand, flipDownCommand);


    @Test
    public void flipUp() {
        // WHEN
        lightSwitcher.flipUp();

        // THEN
        Mockito.verify(light, Mockito.times(1)).turnOn();
    }

    @Test
    public void flipDown() {
        // WHEN
        lightSwitcher.flipDown();

        // THEN
        Mockito.verify(light, Mockito.times(1)).turnOff();
    }

}