package com.epam.rd.summer2019.factoryMethod;

import org.junit.Assert;
import org.junit.Test;

public class FactoryMethodTest {

    @Test
    public void orderPizza() {
        // GIVEN
        PizzaStore nyPizzaStore = new NYPizzaStore();
        Pizza pizza = nyPizzaStore.orderPizza(PizzaType.CHEESE);
        // WHEN order pizza
        // THEN
        Assert.assertEquals("NYStyleCheese Pizza", pizza.getName());
        Assert.assertEquals("Thick", pizza.getDough());
        Assert.assertEquals("Extra Cheese Pizza Sauce", pizza.getSauce());
    }

    @Test
    public void orderNyVeggie() {
        // GIVEN
        PizzaStore nyPizzaStore = new NYPizzaStore();
        Pizza pizza = nyPizzaStore.orderPizza(PizzaType.VEGGIE);
        // WHEN order pizza
        // THEN
        Assert.assertEquals("NYStyleVeggie Pizza", pizza.getName());
        Assert.assertEquals("Extra Thick Crust", pizza.getDough());
        Assert.assertEquals("Marinara sauce", pizza.getSauce());
    }

    @Test
    public void orderChicagoVeggie() {
        // GIVEN
        PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        Pizza pizza = chicagoPizzaStore.orderPizza(PizzaType.VEGGIE);
        // WHEN order pizza
        // THEN
        Assert.assertEquals("ChicagoStyleVeggie Pizza", pizza.getName());
        Assert.assertEquals("Thin Crust", pizza.getDough());
        Assert.assertEquals("Marinara sauce", pizza.getSauce());
    }

    @Test
    public void orderChicagoCheese() {
        // GIVEN
        PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        Pizza pizza = chicagoPizzaStore.orderPizza(PizzaType.CHEESE);
        // WHEN order pizza
        // THEN
        Assert.assertEquals("ChicagoStyleCheese Pizza", pizza.getName());
        Assert.assertEquals("Regular Crust", pizza.getDough());
        Assert.assertEquals("Plum Tomato Sauce", pizza.getSauce());
    }

    @Test
    public void wrongPizzaTypeNy() {
        PizzaStore nyPizzaStore = new NYPizzaStore();

        org.junit.Assert.assertThrows(IllegalArgumentException.class, () -> nyPizzaStore.createPizza(PizzaType.TOMATO));
    }

    @Test
    public void wrongPizzaTypeChicago() {
        PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();

        org.junit.Assert.assertThrows(IllegalArgumentException.class, () -> chicagoPizzaStore.createPizza(PizzaType.TOMATO));
    }

}