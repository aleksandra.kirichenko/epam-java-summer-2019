package com.epam.rd.summer2019.builder;

import org.junit.Assert;
import org.junit.Test;

public class PersonTest {
    @Test
    public void build() {
        // GIVEN
        Person person;
        // WHEN
        person = new Person.Builder()
                .setFirstName("Jane")
                .setLastName("Doe")
                .setAge(25)
                .build();

        // THEN
        Assert.assertEquals("Jane", person.getFirstName());
        Assert.assertEquals("Doe", person.getLastName());
        Assert.assertEquals(25, person.getAge());
    }

}