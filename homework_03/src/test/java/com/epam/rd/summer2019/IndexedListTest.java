package com.epam.rd.summer2019;


import org.junit.Test;

public class IndexedListTest {

    @Test
    public void addElement() {
        IndexedList<String> collection = generateList(20);
        System.out.println(collection);
        collection.addElement("20", 20);
        System.out.println(collection);
        collection.addElement("-1", 0);
        System.out.println(collection);
    }

    @Test
    public void updateElement() {
        IndexedList<String> indexedList = generateList(20);
        indexedList.updateElement("33333", 3);
        System.out.println(indexedList);
        indexedList.updateElement("00000", 0);
        System.out.println(indexedList);
        indexedList.updateElement("019191919", 19);
        System.out.println(indexedList);
    }

    @Test
    public void getElement() {
        IndexedList<String> indexedList = generateList(20);
        System.out.println(indexedList.getElement(0));
        System.out.println(indexedList.getElement(15));
        System.out.println(indexedList.getElement(19));
    }

    @Test
    public void removeElement() {
        IndexedList<String> indexedList = generateList(20);
        System.out.println(indexedList.removeElement(0));
        System.out.println(indexedList.removeElement(18));
        System.out.println(indexedList.removeElement(8));
        System.out.println(indexedList);
    }

    @Test
    public void getSize() {
        IndexedList<String> indexedList = generateList(20);
        System.out.println(indexedList.getSize());
    }

    private IndexedList<String> generateList(int size) {
        IndexedList<String> indexedList = new IndexedList<>();
        for (int i = 0; i < size; i++) {
            indexedList.addElement(String.valueOf(i), i);
        }
        return indexedList;
    }
}