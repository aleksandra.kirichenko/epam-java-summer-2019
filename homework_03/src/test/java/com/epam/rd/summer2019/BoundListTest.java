package com.epam.rd.summer2019;

import org.junit.Test;

import static org.junit.Assert.*;

public class BoundListTest {
    @org.junit.Test
    public void addElement() {
        BoundList<String> collection = generateCollection(20);
        System.out.println(collection);
        collection.addElement("20", 20);
        System.out.println(collection);
        collection.addElement("-1", 0);
        System.out.println(collection);
    }

    @org.junit.Test
    public void updateElement() {
        BoundList<String> indexedList = generateCollection(20);
        indexedList.updateElement("33333", 3);
        System.out.println(indexedList);
        indexedList.updateElement("00000", 0);
        System.out.println(indexedList);
        indexedList.updateElement("019191919", 19);
        System.out.println(indexedList);
    }

    @org.junit.Test
    public void getElement() {
        BoundList<String> indexedList = generateCollection(20);
        System.out.println(indexedList.getElement(0));
        System.out.println(indexedList.getElement(15));
        System.out.println(indexedList.getElement(19));
    }

    @org.junit.Test
    public void removeElement() {
        BoundList<String> indexedList = generateCollection(20);
        System.out.println(indexedList.removeElement(0));
        System.out.println(indexedList.removeElement(18));
        System.out.println(indexedList.removeElement(8));
        System.out.println(indexedList);
    }

    @Test
    public void getSize() {
        BoundList<String> indexedList = generateCollection(20);
        System.out.println(indexedList.getSize());
    }

    private BoundList<String> generateCollection(int size) {
        BoundList<String> collection = new BoundList<>();
        for (int i = 0; i < size; i++) {
            collection.addElement(String.valueOf(i), i);
        }
        return collection;
    }

}