    package com.epam.rd.summer2019;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class Base<E> implements List<E> {
    private static Logger logger = LoggerFactory.getLogger(IndexedList.class.getName());

    protected void assertArrayBounds(int index) {
        assertArrayBounds(index, 0, getSize()-1);
    }

    protected void assertArrayBounds(int index, int lowerBound, int upperBound) {
        if (index < lowerBound || index > upperBound) {
            String message = String.format("Wrong index value %s", index);
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }
}
