package com.epam.rd.summer2019;

import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;

public class IndexedList<E> extends Base<E> implements List<E> {
    private final Integer DEFAULT_SIZE = 10;

    private int capacity;
    private int pointer = 0;

    private Object[] indexedListData;

    public IndexedList(int size) {
        this.capacity = size;
        indexedListData = new Object[capacity];
    }

    public IndexedList() {
        this.capacity = DEFAULT_SIZE;
        indexedListData = new Object[capacity];
    }

    @Override
    public void addElement(E element, int index) {
        assertArrayBounds(index, 0, getSize());
        if (pointer == capacity) {
            capacity = capacity * 2;
            indexedListData = Arrays.copyOf(indexedListData, capacity);
        }
        System.arraycopy(indexedListData, index, indexedListData, index + 1, getSize() - index);
        indexedListData[index] = element;
        pointer++;
    }

    @Override
    public void updateElement(E value, int index) {
        assertArrayBounds(index);
        indexedListData[index] = value;
    }

    @Override
    public E getElement(int index) {
        assertArrayBounds(index);
        return indexedList(index);
    }

    @Override
    public E removeElement(int index) {
        assertArrayBounds(index);
        E ret = indexedList(index);
        if (index + 1 == pointer) {
            indexedListData[index] = null;
        } else {
            System.arraycopy(indexedListData, index + 1, indexedListData, index, indexedListData.length - index - 1);
        }
        pointer--;
        return ret;
    }

    @Override
    public int getSize() {
        return pointer;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ");
        for (int i = 0; i < getSize(); i++) {
            String s = Objects.toString(indexedListData[i]);
            joiner.add(s);
        }
        return String.format("[%s]", joiner.toString());
    }

    @SuppressWarnings("unchecked")
    private E indexedList(int index) {
        return (E) indexedListData[index];
    }
}
