import com.epam.rd.summer2019.model.City;
import com.epam.rd.summer2019.utils.CityUtils;
import com.epam.rd.summer2019.utils.CollectionUtils;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@RunWith(DataProviderRunner.class)
public class CollectionUtilsTest {

    @DataProvider
    public static Object[][] provideStringAndExpectedLength() {
        List<City> cities = new ArrayList<>();
        cities.add(new City("Novomoskovsk", "Dniprovska", 73786));
        cities.add(new City("Dnipro", "Dniprovska", 990960));
        cities.add(new City("Pavlograd", "Dniprovska", 106933));
        cities.add(new City("Pokrov", "Dniprovska", 41239));
        cities.add(new City("Kiev", "Kievska", 2884000));
        cities.add(new City("Brovary", "Kievska", 98250));
        cities.add(new City("Bila Cerkva", "Kievska", 203816));
        cities.add(new City("Irpen", "Kievska", 42924));
        return new Object[][]{
                {cities}
        };
    }


    @Test
    public void zip() {
        Stream<String> zip = CollectionUtils.zip(
                Stream.of("First", "Second", "Third", "Forth", "Fifth"),
                Stream.of("Maxim", "Alexandra", "Danil", "Alisa")
        );
        zip.forEach(System.out::println);
    }

    @Test
    @UseDataProvider("provideStringAndExpectedLength")
    public void getLargestCityPerStateStream(List<City> cities) {
        Map<String, City> largestCityPerStateCollection = CityUtils.getLargestCityPerStateStream(cities);
        largestCityPerStateCollection.forEach((k, v) -> System.out.println(String.format("State: %s, Biggest city: %s", k, v)));
    }

    @Test
    @UseDataProvider("provideStringAndExpectedLength")
    public void getLargestCityPerStateCollection(List<City> cities) {
        Map<String, City> largestCityPerStateCollection = CityUtils.getLargestCityPerStateCollection(cities);
        largestCityPerStateCollection.forEach((k, v) -> System.out.println(String.format("State: %s, Biggest city: %s", k, v)));
    }

    @Test
    public void parity() {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(null);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        System.out.println(CollectionUtils.numbersByParityFormat(numbers));
    }

}