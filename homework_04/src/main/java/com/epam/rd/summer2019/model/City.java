package com.epam.rd.summer2019.model;

public class City {
    private String name;
    private String state;
    private long population;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public City() {
    }

    public City(String name, String state, long population) {
        this.name = name;
        this.state = state;
        this.population = population;
    }

    @Override
    public String toString() {
        return "model.City{" +
                "name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", population=" + population +
                '}';
    }
}
