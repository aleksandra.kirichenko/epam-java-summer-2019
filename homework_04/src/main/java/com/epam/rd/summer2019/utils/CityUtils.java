package com.epam.rd.summer2019.utils;


import com.epam.rd.summer2019.model.City;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.BinaryOperator.maxBy;

public class CityUtils {

    public static Map<String, City> getLargestCityPerStateStream(Collection<City> cities) {
        return cities.stream().collect(Collectors.groupingBy(
                City::getState,
                Collectors.reducing(new City(), maxBy((city1, city2) -> (int) (city1.getPopulation() - city2.getPopulation())))
        ));
    }

    public static Map<String, City> getLargestCityPerStateCollection(Collection<City> cities) {
        Map<String, City> ret = new HashMap<>();
        for (City city : cities) {
            addCityIfApplicable(ret, city);
        }
        return ret;
    }

    private static void addCityIfApplicable(Map<String, City> stateCityMap, City city) {
        City c = stateCityMap.get(city.getState());
        if (c == null || c.getPopulation() < city.getPopulation()) {
            stateCityMap.put(city.getState(), city);
        }
    }

}
