package com.epam.rd.summer2019.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionUtils {

    public static String numbersByParityFormat(List<Integer> numbers) {
        return numbers.stream()
                .filter(Objects::nonNull)
                .map(number ->(number % 2 == 0 ? "e" : "o") + number)
                .collect(Collectors.joining(","));
    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Stream.Builder<T> builder = Stream.builder();
        Iterator<T> firstIterator = first.iterator();
        Iterator<T> secondIterator = second.iterator();
        while (firstIterator.hasNext() && secondIterator.hasNext()) {
            builder.add(firstIterator.next()).add(secondIterator.next());
        }
        return builder.build();
    }

}
