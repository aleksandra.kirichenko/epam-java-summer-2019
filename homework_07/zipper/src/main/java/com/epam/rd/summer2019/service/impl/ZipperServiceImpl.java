package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.model.ZipException;
import com.epam.rd.summer2019.service.ZipperService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipperServiceImpl implements ZipperService {

    @Override
    public String zip(List<String> fileNames) throws IOException {
        if (fileNames == null || fileNames.size() == 0) {
            throw new ZipException("Couldn't zip file. Source file name not specified");
        }

        File originalFile = new File(fileNames.get(0));
        if (!originalFile.exists()) {
            throw new ZipException(String.format("Couldn't zip file. Source File with name %s not exist", originalFile.getName()));
        }

        File zipArchiveFile = new File((fileNames.size() > 1 ? fileNames.get(1) : originalFile.getAbsolutePath()) + ".zip");
        if (Files.exists(zipArchiveFile.toPath())) {
            throw new ZipException(String.format("Couldn't create file with name %s. File already exist", zipArchiveFile.getAbsolutePath()));
        }

        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipArchiveFile))) {
            zipCatalog(originalFile, zipArchiveFile.getName().replaceAll(".zip", ""), zipOutputStream);
        }
        return "Done!";
    }

    private void zipCatalog(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isDirectory()) {
            String zipEntryName = String.format("%s%s", fileName, fileName.endsWith("/") ? "" : "/");
            zipOut.putNextEntry(new ZipEntry(zipEntryName));
            zipOut.closeEntry();
            for (File childFile : Optional.ofNullable(fileToZip.listFiles()).orElse(new File[0])) {
                zipCatalog(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        zipFile(fileToZip.getAbsolutePath(), fileName, zipOut);
    }

    private void zipFile(String fileToZip, String zipEntryName, ZipOutputStream zipOut) throws IOException {
        try (FileInputStream fis = new FileInputStream(fileToZip)) {
            ZipEntry zipEntry = new ZipEntry(zipEntryName);
            zipOut.putNextEntry(zipEntry);
            byte[] buffer = new byte[fis.available()];
            zipOut.write(buffer);
        }
    }

    @Override
    public String unzip(List<String> fileNames) throws IOException {
        String fileZip = fileNames.get(0);
        String destinationFileName = (fileNames.size() > 1 ? fileNames.get(1) : fileNames.get(0)).replaceAll(".zip", "");
        File destDir = new File(destinationFileName);

        try (FileInputStream fis = new FileInputStream(fileZip);
             ZipInputStream zipInputStream = new ZipInputStream(fis)) {
            ZipEntry zipEntry;
            byte[] buffer = new byte[fis.available()];
            while (Objects.nonNull(zipEntry = zipInputStream.getNextEntry())) {
                File newFile = new File(destDir, zipEntry.getName());
                if (zipEntry.getName().endsWith("/")) {
                    newFile.mkdirs();
                } else {
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        fos.write(buffer);
                    }
                }
            }
        }
        return "Done!";
    }
}
