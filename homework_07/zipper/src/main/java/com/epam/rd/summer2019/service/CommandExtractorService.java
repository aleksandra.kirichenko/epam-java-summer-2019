package com.epam.rd.summer2019.service;

import com.epam.rd.summer2019.model.Command;

public interface CommandExtractorService {
    Command extractCommand(String text);
}
