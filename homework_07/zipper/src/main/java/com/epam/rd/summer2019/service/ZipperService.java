package com.epam.rd.summer2019.service;

import java.io.IOException;
import java.util.List;

public interface ZipperService {

    String zip(List<String> fileNames) throws IOException;

    String unzip(List<String> fileNames) throws IOException;
}
