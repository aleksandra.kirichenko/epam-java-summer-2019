package com.epam.rd.summer2019.controller;

import java.io.IOException;

public interface ConsoleController {

    String processCommand(String command) throws IOException;

    boolean isTerminationCommand(String command);
}
