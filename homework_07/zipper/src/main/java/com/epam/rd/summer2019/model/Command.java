package com.epam.rd.summer2019.model;

import java.util.List;

public class Command {
    private String commandName;
    private List<String> parameters;

    public Command(String commandName, List<String> parameters) {
        this.commandName = commandName;
        this.parameters = parameters;
    }

    public String getCommandName() {
        return commandName;
    }

    public List<String> getParameters() {
        return parameters;
    }
}
