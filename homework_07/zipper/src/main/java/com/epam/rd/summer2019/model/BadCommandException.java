package com.epam.rd.summer2019.model;

public class BadCommandException extends RuntimeException {
    public BadCommandException() {
        super();
    }

    public BadCommandException(String message) {
        super(message);
    }

    public BadCommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
