package com.epam.rd.summer2019;

import com.epam.rd.summer2019.controller.ConsoleController;
import com.epam.rd.summer2019.controller.impl.ConsoleControllerImpl;

import java.io.IOException;
import java.util.Scanner;
import java.util.zip.ZipException;

public class ZipperMain {

    public static void main(String[] args) {
        ConsoleController consoleController = new ConsoleControllerImpl();

        System.out.println("zip source_name destination_name (optional): zip file or directory");
        System.out.println("unzip source_name destination_name (optional): unzip file or directory");
        System.out.println("For exit type 'Enter' or 'q'");
        Scanner in = new Scanner(System.in);
        String nextCommand;
        do {
            System.out.println("> ");
            nextCommand = in.nextLine();
            String result = null;
            try {
                result = consoleController.processCommand(nextCommand);
                System.out.println(result);
            } catch (ZipException zip) {
                System.err.println(zip.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!consoleController.isTerminationCommand(nextCommand));
    }
}
