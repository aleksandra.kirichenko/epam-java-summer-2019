package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.model.Command;
import com.epam.rd.summer2019.model.ZipException;
import com.epam.rd.summer2019.service.CommandExtractorService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommandExtractorServiceImpl implements CommandExtractorService {
    @Override
    public Command extractCommand(String text) {
        if (text == null || text.equals("")) {
            throw new ZipException("Command could not be empty");
        }
        String[] commandArray = text.toLowerCase().split(" ");
        List<String> params = new ArrayList<>();
        if (commandArray.length > 1) {
            params.addAll(Stream.of(Arrays.copyOfRange(commandArray, 1, commandArray.length)).collect(Collectors.toList()));
        }
        return new Command(commandArray[0], params);
    }
}
