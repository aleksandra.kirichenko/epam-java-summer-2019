package com.epam.rd.summer2019.controller.impl;

import com.epam.rd.summer2019.controller.ConsoleController;
import com.epam.rd.summer2019.model.Command;
import com.epam.rd.summer2019.model.ZipException;
import com.epam.rd.summer2019.service.CommandExtractorService;
import com.epam.rd.summer2019.service.ZipperService;
import com.epam.rd.summer2019.service.impl.CommandExtractorServiceImpl;
import com.epam.rd.summer2019.service.impl.ZipperServiceImpl;

import java.io.IOException;

public class ConsoleControllerImpl implements ConsoleController {
    private ZipperService zipperService;
    private CommandExtractorService commandExtractorService;

    public ConsoleControllerImpl() {
        this.zipperService = new ZipperServiceImpl();
        this.commandExtractorService = new CommandExtractorServiceImpl();
    }

    @Override
    public String processCommand(String text) throws IOException {

        Command command = commandExtractorService.extractCommand(text);
        switch (command.getCommandName()) {
            case "zip":
                return zipperService.zip(command.getParameters());
            case "unzip":
                return zipperService.unzip(command.getParameters());
            default:
                throw new ZipException(String.format("Unknown command: %s", command.getCommandName()));
        }

    }

    @Override
    public boolean isTerminationCommand(String command) {
        return command.equals("") || command.equalsIgnoreCase("q");
    }
}
