package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.model.Command;
import com.epam.rd.summer2019.model.ZipException;
import com.epam.rd.summer2019.service.CommandExtractorService;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.junit.MatcherAssert.assertThat;

public class CommandExtractorServiceImplTest {

    private CommandExtractorService commandExtractorService = new CommandExtractorServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void extractZip() {
        // GIVEN
        String commandText = "zip D:/test.txt D:/text_zipped.txt";

        // WHEN
        Command command = commandExtractorService.extractCommand(commandText);

        // THAN
        Assert.assertEquals("zip", command.getCommandName());
        assertThat(command.getParameters(), Matchers.contains("d:/test.txt", "d:/text_zipped.txt"));

    }

    @Test
    public void extractUnzip() {
        // GIVEN
        String commandText = "unzip D:/test.txt D:/text_zipped.txt";

        // WHEN
        Command command = commandExtractorService.extractCommand(commandText);

        // THAN
        Assert.assertEquals("unzip", command.getCommandName());
        assertThat(command.getParameters(), Matchers.contains("d:/test.txt", "d:/text_zipped.txt"));
    }

    @Test
    public void extractError() {
        // GIVEN
        String commandText = "";

        // THAN
        thrown.expect(ZipException.class);
        thrown.expectMessage("Command could not be empty");

        // WHEN
        commandExtractorService.extractCommand(commandText);
    }
}