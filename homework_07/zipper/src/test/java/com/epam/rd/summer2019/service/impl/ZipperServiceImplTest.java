package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.model.ZipException;
import com.epam.rd.summer2019.service.ZipperService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ZipperServiceImplTest {
    ZipperService zipperService = new ZipperServiceImpl();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void sourceNotExistException() throws IOException {
        //GIVEN
        String wrongFileName = "wrong_file.txt";

        //THEN
        thrown.expect(ZipException.class);
        thrown.expectMessage(String.format("Couldn't zip file. Source File with name %s not exist", wrongFileName));

        //WHEN
        zipperService.zip(Stream.of(wrongFileName).collect(Collectors.toList()));
    }

    @Test
    public void fileNameNotSpecifiedException() throws IOException {
        //GIVEN
        List<String> emptyParamList = new ArrayList<>();

        //THEN
        thrown.expect(ZipException.class);
        thrown.expectMessage("Couldn't zip file. Source file name not specified");

        //WHEN
        zipperService.zip(emptyParamList);
    }

    @Test
    public void nameDuplicationException() throws IOException {
        //GIVEN
        File zipDuplicate = temporaryFolder.newFile("output.txt.zip");
        File source = temporaryFolder.newFile("output.txt");

        //THEN
        thrown.expect(ZipException.class);
        thrown.expectMessage(String.format("Couldn't create file with name %s. File already exist", zipDuplicate.getAbsolutePath()));

        //WHEN
        zipperService.zip(Stream.of(source.getAbsolutePath()).collect(Collectors.toList()));
    }

    @Test
    public void fileZipped() throws IOException {
        // GIVEN
        File output = temporaryFolder.newFile("output.txt");

        // WHEN
        zipperService.zip(Stream.of(output.getAbsolutePath()).collect(Collectors.toList()));

        // THAN
        Assert.assertTrue(new File(output.getAbsolutePath() + ".zip").exists());
    }

    @Test
    public void directoryZipped() throws IOException {
        // GIVEN
        File directory = temporaryFolder.newFolder("test_folder");
        File innerFile = new File(directory, "inner-file.txt");
        Files.createFile(innerFile.toPath());

        // WHEN
        zipperService.zip(Stream.of(directory.getAbsolutePath()).collect(Collectors.toList()));

        // THAN
        Assert.assertTrue(new File(directory.getAbsolutePath() + ".zip").exists());
    }

    @Test
    public void directoryUnzipped() throws IOException {
        // GIVEN
        File directory = temporaryFolder.newFolder("test_folder");
        File innerFile = new File(directory, "inner-file.txt");
        Files.createFile(innerFile.toPath());
        zipperService.zip(Stream.of(directory.getAbsolutePath()).collect(Collectors.toList()));
        Files.delete(innerFile.toPath());
        Files.delete(directory.toPath());

        // WHEN
        zipperService.unzip(Stream.of(directory.getAbsolutePath() + ".zip").collect(Collectors.toList()));

        // THAN
        Assert.assertTrue(new File(directory.getAbsolutePath()).exists());
    }

}