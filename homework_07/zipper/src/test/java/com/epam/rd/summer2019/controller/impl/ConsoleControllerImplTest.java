package com.epam.rd.summer2019.controller.impl;

import com.epam.rd.summer2019.model.ZipException;
import com.epam.rd.summer2019.service.ZipperService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ConsoleControllerImplTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    ZipperService zipperService;

    @InjectMocks
    ConsoleControllerImpl controller = new ConsoleControllerImpl();

    @Test
    public void processZipCommand() throws IOException {
        // GIVEN
        String zipCommand = "zip D:/test D:/test";
            doReturn("Done!").when(zipperService).zip(any());

        // WHEN
        String answer = controller.processCommand(zipCommand);

        // THEN
        verify(zipperService, times(1)).zip(any());
        Assert.assertEquals("Done!", answer);
    }

    @Test
    public void processUnzipCommand() throws IOException {
        // GIVEN
        String zipCommand = "unzip D:/test D:/test";
        doReturn("Done!").when(zipperService).unzip(any());

        // WHEN
        String answer = controller.processCommand(zipCommand);

        // THEN
        verify(zipperService, times(1)).unzip(any());
        Assert.assertEquals("Done!", answer);
    }

    @Test
    public void errorIfCommandUnknown() throws IOException {
        // GIVEN
        String zipCommand = "unknown_command D:/test D:/test";

        // THEN
        thrown.expect(ZipException.class);
        thrown.expectMessage("Unknown command: unknown_command");

        // WHEN
        controller.processCommand(zipCommand);
    }

    @Test
    public void isTerminationCommand() {
        // GIVEN
        String terminationCommand = "q";

        // WHEN
        boolean isTermination = controller.isTerminationCommand(terminationCommand);

        // THEN
        Assert.assertTrue(isTermination);
    }
}