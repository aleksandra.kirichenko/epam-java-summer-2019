package com.epam.rd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EchoServlet extends HttpServlet {

    private String message;
    private Map<String, String> additionalMessages = new HashMap<>();


    public void init() throws ServletException {
        message = "Hello World";
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Set response content type
        response.setContentType("text/html");

        // Actual logic goes here.
        final PrintWriter out = response.getWriter();
        out.println("<h1>" + message + "</h1>");

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            Object headerName = headerNames.nextElement();
            String headerValue = request.getHeader((String) headerName);
            out.println(String.format("<h1>%s:%s</h1>", headerName, headerValue));
        }
        if (additionalMessages.size() > 0) {
            out.println("Additional parameters:");
            additionalMessages.forEach((key, value) -> out.println(String.format("<h2>%s: %s</h2>", key, value)));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String userName = getBodyParameter(req);
        additionalMessages.put("username", userName);
        final PrintWriter out = resp.getWriter();
        out.println("Response: " + userName);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String urlParamUserName = req.getParameter("username");
        final String userName = additionalMessages.get("username");
        if(userName != null && userName.equalsIgnoreCase(urlParamUserName)){
            final String bodyParameterUserName = getBodyParameter(req);
            additionalMessages.put("username", bodyParameterUserName);
        }
        final PrintWriter out = resp.getWriter();
        out.println("Response: " + additionalMessages.get("username"));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        additionalMessages.remove("username", req.getParameter("username"));
    }

    public void destroy() {
        // do nothing.
    }

    private String getBodyParameter(HttpServletRequest request) {
        String str;
        StringBuilder wholeStr = new StringBuilder();
        try {
            BufferedReader br = request.getReader();
            while ((str = br.readLine()) != null) {
                wholeStr.append(str);
            }
            return wholeStr.toString();
        } catch (Exception e) {
            // TODO add log
            throw new RuntimeException(e);
        }
    }
}