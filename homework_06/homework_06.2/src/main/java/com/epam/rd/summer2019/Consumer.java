package com.epam.rd.summer2019;

public class Consumer {
    public void consume(String value) {
        System.out.println("Consumed -> " + value);
    }
}
