package com.epam.rd.summer2019;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProcessorTest {

    @Mock
    Producer producer;
    @Mock
    Consumer consumer;
    @Captor
    ArgumentCaptor<String> stringCapture;
    @InjectMocks
    Processor processor;

    @Test
    public void consumerWasCalledWithArgumentProducedByProducer() {
        //GIVEN
        when(producer.produce()).thenReturn("produced value");

        //WHEN
        processor.process();

        //THEN
        verify(consumer).consume(stringCapture.capture());
        Assert.assertEquals("produced value", stringCapture.getValue());
    }

    @Test(expected = IllegalStateException.class)
    public void processorThrowExceptionIfProducerNotProduce(){
        //GIVEN
        when(producer.produce()).thenReturn(null);

        //WHEN
        processor.process();

        //THAN IllegalStateException occur
    }

}