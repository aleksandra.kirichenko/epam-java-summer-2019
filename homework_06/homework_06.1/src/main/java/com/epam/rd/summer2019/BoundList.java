package com.epam.rd.summer2019;

public class BoundList<E> extends Base<E> implements Bounnd<E> {

    private Node first;
    private Node last;
    private int size;

    @Override
    public void addElementToStart(E element) {
        Node f = first;
        Node newNode = new Node(null, element, first);
        if (size == 0) {
            last = newNode;
        } else {
            f.prev = newNode;
        }
        first = newNode;
        size++;
    }

    @Override
    public void addElementToEnd(E element) {
        Node l = last;
        Node newNode = new Node(last, element, null);
        if (size == 0) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        last = newNode;
        size++;
    }

    @Override
    public E getFirstElement() {
        assertArrayBounds(0);
        return first.value;
    }

    @Override
    public E getLastElement() {
        assertArrayBounds(getSize() == 0 ? 0 : getSize() - 1);
        return last.value;
    }

    @Override
    public void addElement(E element, int index) {
        assertArrayBounds(index, getSize());
        if (index == 0) {
            addElementToStart(element);
        } else if (index == getSize()) {
            addElementToEnd(element);
        } else {
            Node next = getNodeFromStart(index);
            Node prev = next.prev;
            Node newNode = new Node(prev, element, next);

            prev.next = newNode;
            next.prev = newNode;

            size++;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public E  getElement(int index) {
        assertArrayBounds(index);
        return getNodeFromStart(index).value;
    }

    @Override
    public E removeElement(int index) {
        assertArrayBounds(index);
        Node removed;
        if (index == 0) {
            removed = first;
            Node next = first.next;
            next.prev = null;
            first = next;
        } else if (index == size - 1) {
            removed = last;
            Node prev = last.prev;
            prev.next = null;
            last = prev;
        } else {
            removed = getNodeFromStart(index);
            Node next = removed.next;
            Node prev = removed.prev;
            prev.next = next;
            next.prev = prev;
        }
        size--;
        return removed.value;
    }

    @Override
    public void updateElement(E value, int index) {
        assertArrayBounds(index);
        getNodeFromStart(index).value = value;
    }

    private Node getNodeFromStart(int index) {
        assertArrayBounds(index, getSize());
        Node node = first;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    class Node {

        Node prev;
        E value;
        Node next;

        Node(Node prev, E value, Node next) {
            this.prev = prev;
            this.value = value;
            this.next = next;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int size = getSize();
        Node current = first;
        for (int i = 0; i < size; i++) {
            stringBuilder.append(current.value);
            if (i != size - 1) {
                stringBuilder.append(", ");
            }
            current = current.next;
        }

        return String.format("[%s]", stringBuilder.toString());
    }
}
