package com.epam.rd.summer2019;

public interface Bounnd<E> extends List<E> {
    void addElementToStart(E element);

    void addElementToEnd(E element);

    E getFirstElement();

    E getLastElement();

}
