    package com.epam.rd.summer2019;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class Base<E> implements List<E> {
    private static Logger logger = LoggerFactory.getLogger(IndexedList.class.getName());

    protected void assertArrayBounds(int index) {
        assertArrayBounds(index, getSize()-1);
    }

    protected void assertArrayBounds(int index, int upperBound) {
        if (index < 0 || index > upperBound) {
            String message = String.format("Wrong index value %s", index);
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }
}
