package com.epam.rd.summer2019;

public interface List<E> {
    int getSize();

    E getElement(int index);

    void addElement(E value, int index);

    E removeElement(int index);

    void updateElement(E value, int index);
}
