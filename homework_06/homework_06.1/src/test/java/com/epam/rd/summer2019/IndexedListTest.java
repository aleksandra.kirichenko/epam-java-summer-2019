package com.epam.rd.summer2019;

import org.junit.Test;
import org.testng.Assert;

public class IndexedListTest {

    @Test
    public void internalArrayOfListIncreased() {
        //GIVEN
        IndexedList<String> list = new IndexedList<>(10);
        // WHEN
        for (int i = 0; i < 100; i++) {
            list.addElement(String.valueOf(i), i);
        }
        // THEN
        Assert.assertEquals(list.getSize(), 100);
    }

}