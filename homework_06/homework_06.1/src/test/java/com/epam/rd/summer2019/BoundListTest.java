package com.epam.rd.summer2019;

import org.junit.Test;
import org.testng.Assert;


public class BoundListTest {

    @Test
    public void addElementAtEnd() {
        //GIVEN
        BoundList<String> list = new BoundList<>();
        list.addElementToEnd("First");
        list.addElementToEnd("Second");
        list.addElementToEnd("Third");
        list.addElementToEnd("Forth");
        // WHEN
        list.addElementToEnd("Last");
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Third, Forth, Last]");
    }

    @Test
    public void addElementAtStart() {
        //GIVEN
        BoundList<String> list = new BoundList<>();
        list.addElementToEnd("Second");
        list.addElementToEnd("Third");
        list.addElementToEnd("Forth");
        list.addElementToEnd("Last");
        // WHEN
        list.addElementToStart("First");
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Third, Forth, Last]");
    }

    @Test
    public void getFirstElement() {
        //GIVEN
        BoundList<String> list = new BoundList<>();
        list.addElementToStart("First");
        list.addElementToEnd("Second");
        list.addElementToEnd("Third");
        list.addElementToEnd("Forth");
        list.addElementToEnd("Last");
        // WHEN
        String first = list.getFirstElement();
        // THEN
        Assert.assertEquals(first, "First");
    }

    @Test
    public void getLastElement() {
        //GIVEN
        BoundList<String> list = new BoundList<>();
        list.addElementToStart("First");
        list.addElementToEnd("Second");
        list.addElementToEnd("Third");
        list.addElementToEnd("Forth");
        list.addElementToEnd("Last");
        // WHEN
        String last = list.getLastElement();
        // THEN
        Assert.assertEquals(last, "Last");
    }

}