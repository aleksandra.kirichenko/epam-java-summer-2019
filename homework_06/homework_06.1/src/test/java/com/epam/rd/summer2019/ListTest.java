package com.epam.rd.summer2019;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.testng.Assert;

@RunWith(Parameterized.class)
public class ListTest {

    private List<String> list;

    public ListTest(List<String> list) {
        this.list = list;
    }

    @Parameterized.Parameters(name = "{index}: listTest - {0}")
    public static Object[][] data() {
        return new Object[][]{
                {new IndexedList<String>()},
                {new BoundList<String>()}
        };
    }

    @Test
    public void addElementAtStartOfEmptyList() {
        //GIVEN
        list = cleanList(list);
        // WHEN
        list.addElement("Added to start element", 0);
        // THEN
        Assert.assertEquals("Added to start element", list.getElement(0));
    }

    @Test
    public void addElementAtStartOfNotEmptyList() {
        //GIVEN
        list = cleanList(list);
        for (int i = 0; i < 100; i++) {
            list.addElement(String.valueOf(i), i);
        }
        // WHEN
        list.addElement("Added to start element", 0);
        // THEN
        Assert.assertEquals("Added to start element", list.getElement(0));
    }

    @Test
    public void addElementAtEndOfList() {
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Third", 2);
        list.addElement("Forth", 3);
        // WHEN
        list.addElement("Added last element", list.getSize());
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Third, Forth, Added last element]");
    }

    @Test
    public void addElementAtMiddleOfList() {
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Third", 2);
        list.addElement("Forth", 3);
        // WHEN
        list.addElement("Middle", 2);
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Middle, Third, Forth]");
    }

    @Test
    public void removeFirstElement(){
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Third", 2);
        list.addElement("Forth", 3);
        // WHEN
        list.removeElement(0);
        // THEN
        Assert.assertEquals(list.toString(), "[Second, Third, Forth]");
    }

    @Test
    public void removeLastElement(){
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Third", 2);
        list.addElement("Forth", 3);
        // WHEN
        list.removeElement(list.getSize()-1);
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Third]");
    }

    @Test
    public void removeMiddleElement(){
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Middle", 2);
        list.addElement("Third", 3);
        list.addElement("Forth", 4);
        // WHEN
        list.removeElement(2);
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Third, Forth]");
    }

    @Test
    public void updateElement(){
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Middle", 2);
        list.addElement("Third", 3);
        list.addElement("Forth", 4);
        // WHEN
        list.updateElement("Updated", 2);
        // THEN
        Assert.assertEquals(list.toString(), "[First, Second, Updated, Third, Forth]");
    }

    @Test
    public void wrongBoundsFails(){
        //GIVEN
        list = cleanList(list);
        list.addElement("First", 0);
        list.addElement("Second", 1);
        list.addElement("Middle", 2);
        list.addElement("Third", 3);
        list.addElement("Forth", 4);

        //WHEN
        //THEN
        org.junit.Assert.assertThrows(IllegalArgumentException.class, ()->list.getElement(100));
    }

    private List<String> cleanList(List<String> list) {
        try {
            return list.getClass().newInstance();
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
