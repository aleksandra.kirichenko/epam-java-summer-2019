package com.epam.rd.summer2019;

import com.epam.rd.summer2019.model.ParseProperties;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

public class ParseActionTest {
    ParseAction parserAction = new ParseAction();

    @Test
    public void doParseAction() throws ParseException, IOException, ClassNotFoundException {
        ParseProperties properties = new ParseProperties()
                .setSavePath("D:\\saved_results.dat")
                .setMaxModuleSize(10)
                .setLogFilePath("D:\\Саша Лекции\\students summer 2019\\Homeworks\\logs.txt");
        parserAction.doAction(properties);
    }

    @Test
    public void doLoadAction() throws ParseException, IOException, ClassNotFoundException {
        ParseProperties properties = new ParseProperties()
                .setLoadPath("D:\\saved_results.dat")
                .setMaxModuleSize(10)
                .setLogFilePath("D:\\Саша Лекции\\students summer 2019\\Homeworks\\logs.txt");
        parserAction.doAction(properties);
    }
}