package com.epam.rd.summer2019.service;

import com.epam.rd.summer2019.model.LogInfo;
import com.epam.rd.summer2019.model.LogStatisticData;
import com.epam.rd.summer2019.model.ParseProperties;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogStatisticParserServiceImpl implements LogStatisticParserService {
    private final static String REGEX_PATTERN = "^(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s+(TRACE|DEBUG|INFO|WARN|ERROR)\\s+Module=(\\w{3})\\s+Operation: (.*)Execution time:\\s+(\\d+)\\s+ms";


    @Override
    public LogStatisticData parse(ParseProperties parseProperties) throws IOException, ParseException {
        LogStatisticData logStatisticData = new LogStatisticData(parseProperties.getMaxModuleSize());
        try (FileReader fileReader = new FileReader(parseProperties.getLogFilePath());
             BufferedReader reader = new BufferedReader(fileReader)) {
            String line;
            while ((line = reader.readLine()) != null) {
                logStatisticData.add(parseLog(line));
            }
        }
        return logStatisticData;
    }

    @Override
    public LogStatisticData deserialize(ParseProperties parseProperties) throws IOException, ClassNotFoundException {
        try (FileInputStream fileInputStream = new FileInputStream(new File(parseProperties.getLoadPath()));
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            return (LogStatisticData) objectInputStream.readObject();
        }
    }

    @Override
    public void serialize(LogStatisticData logStatisticData, ParseProperties parseProperties) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(new File(parseProperties.getSavePath()));
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {
            objectOutputStream.writeObject(logStatisticData);
        }
    }

    private LogInfo parseLog(String text) throws ParseException {
        Pattern p = Pattern.compile(REGEX_PATTERN);
        Matcher m = p.matcher(text.trim());
        if (!m.matches()) {
            throw new RuntimeException(String.format("Couldn't parse line: %s", text));
        }
        LogInfo result = new LogInfo();
        result.setDate(new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.sss").parse(m.group(1)));
        result.setLogType(m.group(2));
        result.setModule(m.group(3));
        result.setOperation(Optional.ofNullable(m.group(4)).map(String::trim).orElse(null));
        result.setTimeExe(Long.parseLong(m.group(5)));
        return result;
    }

}
