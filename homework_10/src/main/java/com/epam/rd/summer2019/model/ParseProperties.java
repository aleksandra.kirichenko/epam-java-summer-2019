package com.epam.rd.summer2019.model;

public class ParseProperties {
    private String logFilePath;
    private String loadPath;
    private Integer maxModuleSize;
    private String savePath;

    public String getLogFilePath() {
        return logFilePath;
    }

    public ParseProperties setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
        return this;
    }

    public String getLoadPath() {
        return loadPath;
    }

    public ParseProperties setLoadPath(String loadPath) {
        this.loadPath = loadPath;
        return this;
    }

    public Integer getMaxModuleSize() {
        return maxModuleSize;
    }

    public ParseProperties setMaxModuleSize(Integer maxModuleSize) {
        this.maxModuleSize = maxModuleSize;
        return this;
    }

    public String getSavePath() {
        return savePath;
    }

    public ParseProperties setSavePath(String savePath) {
        this.savePath = savePath;
        return this;
    }

    @Override
    public String toString() {
        return "ParseProperties{" +
                "logFilePath='" + logFilePath + '\'' +
                ", loadPath='" + loadPath + '\'' +
                ", maxModuleSize=" + maxModuleSize +
                ", savePath='" + savePath + '\'' +
                '}';
    }
}
