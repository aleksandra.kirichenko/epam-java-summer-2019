package com.epam.rd.summer2019.utils;

import com.epam.rd.summer2019.model.ParseProperties;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParsePropertiesUtils {

    private ParsePropertiesUtils() {
    }

    public static ParseProperties parse(String[] args) {
        String propertiesText = String.join(" ", args);
        return new ParseProperties()
                .setLogFilePath(parseLogFilePath(propertiesText))
                .setLoadPath(parseLoadPath(propertiesText))
                .setSavePath(parseSavePath(propertiesText))
                .setMaxModuleSize(parseMaxModuleSize(propertiesText));
    }

    private static Integer parseMaxModuleSize(String propertiesText) {
        String regex = "–view\\s(\\d+)";
        return Optional.ofNullable(parseValue(propertiesText, regex)).map(Integer::parseInt).orElse(null);
    }

    private static String parseSavePath(String propertiesText) {
        String regex = "-saved\\s([a-zA-Z0-9\\s_\\\\.\\-\\(\\):]+dat)";
        return parseValue(propertiesText, regex);
    }

    private static String parseLoadPath(String propertiesText) {
        String regex = "-load\\s([a-zA-Z0-9\\s_\\\\.\\-\\(\\):]+dat)";
        return parseValue(propertiesText, regex);
    }

    private static String parseLogFilePath(String propertiesText) {
        String regex = "-file\\s([a-zA-Z0-9\\s_\\\\.\\-\\(\\):]+txt)";
        return parseValue(propertiesText, regex);
    }

    private static String parseValue(String propertiesText, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(propertiesText);
        while (m.find()) {
            return m.group(1);
        }
        return null;
    }
}
