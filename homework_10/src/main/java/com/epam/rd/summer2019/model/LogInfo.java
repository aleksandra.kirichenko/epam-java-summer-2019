package com.epam.rd.summer2019.model;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogInfo implements Serializable {
    private Date date;
    private String logType;
    private String module;
    private String operation;
    private long timeExe;

    public Date getDate() {
        return date;
    }

    public LogInfo setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getLogType() {
        return logType;
    }

    public LogInfo setLogType(String logType) {
        this.logType = logType;
        return this;
    }

    public String getModule() {
        return module;
    }

    public LogInfo setModule(String module) {
        this.module = module;
        return this;
    }

    public String getOperation() {
        return operation;
    }

    public LogInfo setOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public long getTimeExe() {
        return timeExe;
    }

    public LogInfo setTimeExe(long timeExe) {
        this.timeExe = timeExe;
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s %s ms, finished at %s", this.operation, this.timeExe,  new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").format(this.date));
    }
}
