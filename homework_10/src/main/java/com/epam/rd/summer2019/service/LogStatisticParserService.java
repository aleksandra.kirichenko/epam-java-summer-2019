package com.epam.rd.summer2019.service;

import com.epam.rd.summer2019.model.LogStatisticData;
import com.epam.rd.summer2019.model.ParseProperties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

public interface LogStatisticParserService {

    LogStatisticData parse(ParseProperties parseProperties) throws IOException, ParseException;

    LogStatisticData deserialize(ParseProperties parseProperties) throws IOException, ClassNotFoundException;

    void serialize(LogStatisticData logStatisticData, ParseProperties parseProperties) throws IOException;
}
