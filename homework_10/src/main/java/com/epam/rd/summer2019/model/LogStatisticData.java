package com.epam.rd.summer2019.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class LogStatisticData implements Serializable {
    private int maxSize;
    private Map<String, LinkedList<LogInfo>> data;

    public LogStatisticData(int maxSize) {
        data = new TreeMap<>();
        this.maxSize = maxSize;
    }

    public void add(LogInfo logInfo) {
        LinkedList<LogInfo> logInfos = data.get(logInfo.getModule());
        if (logInfos == null) {
            logInfos = new LinkedList<>();
        }
        add(logInfo, logInfos);
        data.put(logInfo.getModule(), logInfos);
    }

    private void add(LogInfo logInfo, LinkedList<LogInfo> logInfos) {
        logInfos.add(logInfo);
        logInfos.sort(Comparator.comparingLong(LogInfo::getTimeExe).reversed());
        if (logInfos.size() > maxSize) {
            logInfos.removeLast();
        }
    }

    @Override
    public String toString() {
        return data.entrySet().stream().map(entry ->
                String.format("%s:\n%s", entry.getKey(), entry.getValue().stream().map(LogInfo::toString).collect(Collectors.joining("\n")))
        ).collect(Collectors.joining("\n"));
    }
}
