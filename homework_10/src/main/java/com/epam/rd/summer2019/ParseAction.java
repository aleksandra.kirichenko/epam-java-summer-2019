package com.epam.rd.summer2019;

import com.epam.rd.summer2019.model.LogStatisticData;
import com.epam.rd.summer2019.model.ParseProperties;
import com.epam.rd.summer2019.service.LogStatisticParserService;
import com.epam.rd.summer2019.service.LogStatisticParserServiceImpl;
import com.epam.rd.summer2019.utils.ParsePropertiesUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.ParseException;

public class ParseAction {
    LogStatisticParserService logStatisticParserService = new LogStatisticParserServiceImpl();

    public static void main(String[] args) {
        ParseAction action = new ParseAction();
        ParseProperties properties = ParsePropertiesUtils.parse(args);
        action.doAction(properties);
    }

    void doAction(ParseProperties properties) {
        try {
            LogStatisticData logStatisticData = StringUtils.isNotEmpty(properties.getLoadPath()) ?
                    logStatisticParserService.deserialize(properties) : logStatisticParserService.parse(properties);
            System.out.println(logStatisticData);
            if (StringUtils.isNotBlank(properties.getSavePath())) {
                logStatisticParserService.serialize(logStatisticData, properties);
            }
        } catch (IOException | ParseException | ClassNotFoundException e) {
            System.err.println("Internal error. Please refer to logfile for details");
        }
    }

}