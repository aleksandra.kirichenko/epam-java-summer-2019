<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">

    <title>BEST - AIRLINE COMPANY</title>
</head>

<body>
<div class="container">
    `
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h1>Log In</h1>
        </div>
    </div>

    <div class="row">
        <form class="col-6" action="${pageContext.request.contextPath}/login" method="post">
            <div class="form-group">
                <label for="login">Login:</label>
                <input type="text" name="login" class="form-control" id="login"/>
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control" id="password"/>
            </div>
            <button type="submit" class="btn btn-primary">Log In</button>
        </form>
    </div>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>

</body>
</html>


