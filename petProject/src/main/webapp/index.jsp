<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">

    <title>BEST - AIRLINE COMPANY</title>
</head>

<body>
<c:set var="user" scope="session" value="${sessionScope.user}"/>

<div class="container">
    `
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h1>Hello user</h1>
        </div>
    </div>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>

</body>
</html>


