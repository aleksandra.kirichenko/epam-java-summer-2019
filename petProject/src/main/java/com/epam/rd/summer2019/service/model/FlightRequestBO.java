package com.epam.rd.summer2019.service.model;

import com.epam.rd.summer2019.repository.model.Airplane;
import com.epam.rd.summer2019.repository.model.City;
import com.epam.rd.summer2019.repository.model.Employee;

import java.util.Date;
import java.util.Objects;

public class FlightRequestBO {
    private int flightId;
    private Airplane airplane;
    private Date departureDateTime;
    private Date destinationDateTime;
    private City departureCity;
    private City destinationCity;
    private Employee reporter;
    private Employee assignee;

    public Airplane getAirplane() {
        return airplane;
    }

    public FlightRequestBO setAirplane(Airplane airplane) {
        this.airplane = airplane;
        return this;
    }

    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    public FlightRequestBO setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
        return this;
    }

    public Date getDestinationDateTime() {
        return destinationDateTime;
    }

    public FlightRequestBO setDestinationDateTime(Date destinationDateTime) {
        this.destinationDateTime = destinationDateTime;
        return this;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public FlightRequestBO setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
        return this;
    }

    public City getDestinationCity() {
        return destinationCity;
    }

    public FlightRequestBO setDestinationCity(City destinationCity) {
        this.destinationCity = destinationCity;
        return this;
    }

    public int getFlightId() {
        return flightId;
    }

    public FlightRequestBO setFlightId(int flightId) {
        this.flightId = flightId;
        return this;
    }

    public Employee getReporter() {
        return reporter;
    }

    public FlightRequestBO setReporter(Employee reporter) {
        this.reporter = reporter;
        return this;
    }

    public Employee getAssignee() {
        return assignee;
    }

    public FlightRequestBO setAssignee(Employee assignee) {
        this.assignee = assignee;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlightRequestBO)) return false;
        FlightRequestBO that = (FlightRequestBO) o;
        return getFlightId() == that.getFlightId() &&
                getAirplane().equals(that.getAirplane()) &&
                getReporter().equals(that.getReporter()) &&
                getAssignee().equals(that.getAssignee());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlightId(), getAirplane(), getReporter(), getAssignee());
    }

    @Override
    public String toString() {
        return "FlightRequestBO{" +
                "flightId=" + flightId +
                ", airplane=" + airplane +
                ", departureDateTime=" + departureDateTime +
                ", destinationDateTime=" + destinationDateTime +
                ", reporter=" + reporter +
                ", assignee=" + assignee +
                '}';
    }
}
