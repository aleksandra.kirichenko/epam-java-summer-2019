package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.AirlinePosition;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface AirlinePositionRepository {

    void add(AirlinePosition airlinePosition);

    void add(List<AirlinePosition>  airlinePositions);

    AirlinePosition get(int id) ;

    List<AirlinePosition> getAll();

    void update(AirlinePosition airlinePosition);

    void update(List<AirlinePosition>  airlinePositions) ;

    void delete(int id);
}
