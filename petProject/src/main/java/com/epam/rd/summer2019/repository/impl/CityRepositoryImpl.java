package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.CityRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.City;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CityRepositoryImpl implements CityRepository {

    @Override
    public void add(City city) {
        String sqlQuery = "INSERT INTO city (country_code, name) VALUES (? , ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, city.getCountryCode());
            stat.setString(1, city.getName());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add city: %s", city));
        }
    }

    @Override
    public void add(List<City> cities) {
        String sqlQuery = "INSERT INTO city (country_code, name) VALUES (? , ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (City city : cities) {
                stat.setString(1, city.getCountryCode());
                stat.setString(1, city.getName());
                stat.addBatch();
            }
            stat.executeBatch();
        }catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add cities: %s", cities));
        }
    }

    @Override
    public City get(int id) {
        City city = null;
        String sqlQuery = "SELECT * FROM city WHERE city.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    city = new City()
                            .setId(result.getInt(1))
                            .setCountryCode(result.getString(2))
                            .setName(result.getString(3));
            }

        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't get city by id: %s", id));
        }

        return city;
    }

    @Override
    public List<City> getAll() {
        List<City> cities = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM city")) {
                while (result.next()) {
                    City city = new City()
                            .setId(result.getInt(1))
                            .setCountryCode(result.getString(2))
                            .setName(result.getString(3));
                    cities.add(city);
                }
            }
        }catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException("Couldn't get all cities");
        }
        return cities;
    }

    @Override
    public void update(City city) {
        String sqlQuery = "UPDATE city SET country_code=?, name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, city.getCountryCode());
            stat.setString(1, city.getName());
            stat.setInt(3, city.getId());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException (String.format("Couldn't update city: %s", city));
        }

    }

    @Override
    public void update(List<City> cities) {
        String sqlQuery = "UPDATE city SET country_code=?, name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (City city : cities) {
                stat.setString(1, city.getCountryCode());
                stat.setString(1, city.getName());
                stat.setInt(3, city.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't update  cities: %s", cities));
        }
    }

    @Override
    public void delete(int id) {
        String sqlQuery = "DELETE FROM city WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't delete city by id: %s", id));
        }
    }
}
