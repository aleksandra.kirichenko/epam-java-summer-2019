package com.epam.rd.summer2019.service.converter;

import com.epam.rd.summer2019.repository.model.AirlinePosition;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.service.model.ApplicationUser;

public class ApplicationUserConverter {
    public ApplicationUser toApplicationUser(Employee employee, AirlinePosition position) {
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setRole(position.getName());
        applicationUser.setUserName(employee.getName());
        return applicationUser;
    }
}
