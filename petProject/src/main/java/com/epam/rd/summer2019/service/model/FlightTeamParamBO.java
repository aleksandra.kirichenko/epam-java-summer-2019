package com.epam.rd.summer2019.service.model;

import java.util.HashMap;
import java.util.Map;

public class FlightTeamParamBO {
    private Map<String, Integer> teamParamMaps = new HashMap<>();

    public Map<String, Integer> getTeamParamMaps() {
        return teamParamMaps;
    }

    public FlightTeamParamBO setTeamParamMaps(Map<String, Integer> teamParamMaps) {
        this.teamParamMaps = teamParamMaps;
        return this;
    }
}
