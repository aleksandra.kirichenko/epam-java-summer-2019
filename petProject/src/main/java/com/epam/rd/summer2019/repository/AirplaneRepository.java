package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.Airplane;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface AirplaneRepository {

    void add(Airplane airplane);

    void add(List<Airplane> airplanes);

    Airplane get(int id);

    List<Airplane> getAll();

    void update(Airplane airplane);

    void update(List<Airplane> airplanes);

    void delete(int id);
}
