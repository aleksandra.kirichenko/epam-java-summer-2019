package com.epam.rd.summer2019.web.servlet;

import com.epam.rd.summer2019.service.FlightScheduleService;
import com.epam.rd.summer2019.service.impl.FlightScheduleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "airlineAdminServlet", urlPatterns = "/admin")
public class AirlineAdminServlet extends HttpServlet {
    private final FlightScheduleService flightScheduleService = new FlightScheduleServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightScheduleService.getRequested();
        req.getRequestDispatcher("airline-admin.jsp").forward(req, resp);
    }
}
