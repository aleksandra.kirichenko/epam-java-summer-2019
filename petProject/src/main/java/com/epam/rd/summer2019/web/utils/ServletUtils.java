package com.epam.rd.summer2019.web.utils;

import javax.servlet.http.HttpServletRequest;

public class ServletUtils {
    public static String getBaseUrl(HttpServletRequest request){
        String url = request.getRequestURL().toString();
        return url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath();
    }

    public static int getCurrentLoggedInUser() {
        return 1;
    }
}
