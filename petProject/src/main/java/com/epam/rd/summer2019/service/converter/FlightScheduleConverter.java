package com.epam.rd.summer2019.service.converter;

import com.epam.rd.summer2019.repository.AirlinePositionRepository;
import com.epam.rd.summer2019.repository.EmployeeRepository;
import com.epam.rd.summer2019.repository.impl.AirlinePositionRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.EmployeeRepositoryImpl;
import com.epam.rd.summer2019.repository.model.AirlinePosition;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.repository.model.FlightSchedule;
import com.epam.rd.summer2019.service.FlightRequestService;
import com.epam.rd.summer2019.service.impl.FlightRequestServiceImpl;
import com.epam.rd.summer2019.service.model.FlightRequestBO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FlightScheduleConverter {
    private EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    private FlightRequestService flightRequestService = new FlightRequestServiceImpl();
    private AirlinePositionRepository airlinePositionRepository = new AirlinePositionRepositoryImpl();

    private final List<String> availablePositions;

    public FlightScheduleConverter() {
        List<AirlinePosition> positions = airlinePositionRepository.getAll();
        availablePositions = positions.stream().map(AirlinePosition::getName).collect(Collectors.toList());
    }

    public List<FlightSchedule> convert(Map<String, String[]> scheduleParam) {
        List<FlightSchedule> flightSchedules = new ArrayList<>();
        String flightIds = getSingleParamValue(scheduleParam, "flightId");
        if (flightIds == null) {
            return flightSchedules;
        }
        int flightId = Integer.parseInt(flightIds);

        for (Map.Entry<String, String[]> entry : scheduleParam.entrySet()) {
            if (isNotPosition(entry.getKey())) {
                continue;
            }
            for (String login : entry.getValue()) {
                flightSchedules.add(convert(login, flightId));
            }
        }
        return flightSchedules;
    }

    private String getSingleParamValue(Map<String, String[]> scheduleParam, String key) {
        String[] strings = scheduleParam.get(key);
        if (strings == null || strings.length == 0) {
            return null;
        }
        return strings[0];
    }

    private FlightSchedule convert(String login, Integer flightId) {
        Employee employee = employeeRepository.getByLogin(login);
        if (employee == null) {
            throw new IllegalStateException(String.format("Couldn't find employee with login: %s", login));
        }
        FlightRequestBO flightRequestBO = flightRequestService.findById(flightId);
        FlightSchedule flightSchedule = new FlightSchedule();
        flightSchedule.setFlightId(flightId);
        flightSchedule.setEmployeeId(employee.getId());
        flightSchedule.setStartDateTime(new Timestamp(flightRequestBO.getDepartureDateTime().getTime()));
        flightSchedule.setEndDateTime(new Timestamp(flightRequestBO.getDestinationDateTime().getTime()));
        flightSchedule.setDestinationCityId(flightRequestBO.getDestinationCity().getId());
        flightSchedule.setDepartureCityId(flightRequestBO.getDepartureCity().getId());
        flightSchedule.setStatus("requested");
        return flightSchedule;
    }

    private boolean isNotPosition(String text) {
        return !availablePositions.contains(text);
    }

}
