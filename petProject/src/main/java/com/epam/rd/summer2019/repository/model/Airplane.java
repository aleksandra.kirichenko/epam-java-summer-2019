package com.epam.rd.summer2019.repository.model;

public class Airplane {
    private Integer id;
    private String name;
    private int passenger;

    public int getId() {
        return id;
    }

    public Airplane setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Airplane setName(String name) {
        this.name = name;
        return this;
    }

    public int getPassenger() {
        return passenger;
    }

    public Airplane setPassenger(int passenger) {
        this.passenger = passenger;
        return this;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", passenger=" + passenger +
                '}';
    }
}
