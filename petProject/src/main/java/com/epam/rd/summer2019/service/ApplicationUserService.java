package com.epam.rd.summer2019.service;

import com.epam.rd.summer2019.service.model.ApplicationUser;

public interface ApplicationUserService {
    ApplicationUser getApplicationUserByName(String name);

    boolean permissionDenied(ApplicationUser user, String requestURI);

    ApplicationUser getByNamePassword(String name, String password);
}
