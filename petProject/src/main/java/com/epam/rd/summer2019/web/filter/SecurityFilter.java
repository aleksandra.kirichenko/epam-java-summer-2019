package com.epam.rd.summer2019.web.filter;

import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.service.ApplicationUserService;
import com.epam.rd.summer2019.service.impl.ApplicationUserServiceImpl;
import com.epam.rd.summer2019.service.model.ApplicationUser;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@WebFilter("/*")
public class SecurityFilter implements Filter {

    private final ApplicationUserService userService = new ApplicationUserServiceImpl();


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpRequest.getSession(false);
        ApplicationUser user = (ApplicationUser) Optional.ofNullable(session)
                .map(s -> s.getAttribute("user")).orElse(null);
        String requestURI = httpRequest.getRequestURI();
        if (!requestURI.endsWith("login")) {
            if (user==null) {
                httpRequest.getRequestDispatcher("/login").forward(httpRequest, httpResponse);
            } else if(userService.permissionDenied(user, requestURI)){
                httpResponse.sendError(503, String.format("Permission denied for user with role: %s", user.getRole()));
            } else {
                filterChain.doFilter(httpRequest, httpResponse);
            }
        }else {
            filterChain.doFilter(httpRequest, httpResponse);
        }


    }
}
