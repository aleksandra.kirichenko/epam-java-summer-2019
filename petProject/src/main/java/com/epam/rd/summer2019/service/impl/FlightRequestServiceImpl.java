package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.repository.AirlinePositionRepository;
import com.epam.rd.summer2019.repository.TeamParamRepository;
import com.epam.rd.summer2019.repository.TeamRequestRepository;
import com.epam.rd.summer2019.repository.impl.AirlinePositionRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.TeamParamRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.TeamRequestRepositoryImpl;
import com.epam.rd.summer2019.repository.model.AirlinePosition;
import com.epam.rd.summer2019.repository.model.TeamParam;
import com.epam.rd.summer2019.repository.model.TeamRequest;
import com.epam.rd.summer2019.service.FlightRequestService;
import com.epam.rd.summer2019.service.converter.FlightRequestConverter;
import com.epam.rd.summer2019.service.converter.TeamParamConverter;
import com.epam.rd.summer2019.service.model.FlightRequestBO;
import com.epam.rd.summer2019.service.model.FlightTeamParamBO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlightRequestServiceImpl implements FlightRequestService {
    TeamRequestRepository teamRequestRepository = new TeamRequestRepositoryImpl();
    TeamParamRepository teamParamRepository = new TeamParamRepositoryImpl();
    AirlinePositionRepository airlinePositionRepository = new AirlinePositionRepositoryImpl();
    FlightRequestConverter flightRequestConverter = new FlightRequestConverter();

    @Override
    public List<FlightRequestBO> findByAssignee(Integer assigneeId) {
        return flightRequestConverter.toBos(teamRequestRepository.getByAssigneeId(assigneeId));
    }

    @Override
    public FlightTeamParamBO findTeamParam(int airplaneId) {
        TeamParamConverter teamParamConverter = new TeamParamConverter();

        List<TeamParam> teamParams = teamParamRepository.getByAirplaneId(airplaneId);
        Map<Integer, String> positionsTable = airlinePositionRepository.getAll().stream().collect(Collectors.toMap(AirlinePosition::getId, AirlinePosition::getName));
        return teamParamConverter.toFlightTeamParam(teamParams, positionsTable);
    }

    @Override
    public FlightRequestBO findById(int flightId) {
        TeamRequest teamRequest = teamRequestRepository.get(flightId);
        List<TeamRequest> teamRequests = Stream.of(teamRequest).collect(Collectors.toList());
        return flightRequestConverter.toBos(teamRequests).stream().findFirst().orElse(null);
    }
}
