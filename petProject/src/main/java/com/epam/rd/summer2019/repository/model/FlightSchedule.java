package com.epam.rd.summer2019.repository.model;
import java.sql.*;

public class FlightSchedule {
    private int id;
    private int flightId;
    private int employeeId;
    private Timestamp startDateTime;
    private Timestamp endDateTime;
    private int destinationCityId;
    private int departureCityId;
    private String status;

    public int getId() {
        return id;
    }

    public FlightSchedule setId(int id) {
        this.id = id;
        return this;
    }

    public int getFlightId() {
        return flightId;
    }

    public FlightSchedule setFlightId(int flightId) {
        this.flightId = flightId;
        return this;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public FlightSchedule setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
        return this;
    }

    public Timestamp getStartDateTime() {
        return startDateTime;
    }

    public FlightSchedule setStartDateTime(Timestamp startDateTime) {
        this.startDateTime = startDateTime;
        return this;
    }

    public Timestamp getEndDateTime() {
        return endDateTime;
    }

    public FlightSchedule setEndDateTime(Timestamp endDateTime) {
        this.endDateTime = endDateTime;
        return this;
    }

    public int getDestinationCityId() {
        return destinationCityId;
    }

    public FlightSchedule setDestinationCityId(int destinationCityId) {
        this.destinationCityId = destinationCityId;
        return this;
    }

    public int getDepartureCityId() {
        return departureCityId;
    }

    public FlightSchedule setDepartureCityId(int departureCityId) {
        this.departureCityId = departureCityId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public FlightSchedule setStatus(String status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        return "FlightSchedule{" +
                "id=" + id +
                ", flightId=" + flightId +
                ", employeeId=" + employeeId +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                ", destinationCityId=" + destinationCityId +
                ", departureCityId=" + departureCityId +
                ", status='" + status + '\'' +
                '}';
    }
}
