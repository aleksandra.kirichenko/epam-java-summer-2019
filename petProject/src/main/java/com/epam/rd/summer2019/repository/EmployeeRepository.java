package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.Employee;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface EmployeeRepository {

    void add(Employee employee);

    void add(List<Employee> employees);

    Employee get(int id);

    Employee getByLogin(String name);

    List<Employee> getAll();

    void update(Employee employee);

    void update(List<Employee> employees);

    void delete(int id) ;
}
