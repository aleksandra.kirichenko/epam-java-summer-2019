package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.FlightScheduleRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.FlightSchedule;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FlightScheduleRepositoryImpl implements FlightScheduleRepository {
    @Override
    public void add(FlightSchedule flightSchedule) {
        String sqlQuery = "INSERT INTO flight_schedule (team_request_id, employee_id, start_datetime, end_datetime, destination_city_id, departure_city_id, status) VALUES (? , ?, ?, ?, ?, ?, ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, flightSchedule.getFlightId());
            stat.setInt(2, flightSchedule.getEmployeeId());
            stat.setTimestamp(3, flightSchedule.getStartDateTime());
            stat.setTimestamp(4, flightSchedule.getEndDateTime());
            stat.setInt(5, flightSchedule.getDestinationCityId());
            stat.setInt(6, flightSchedule.getDepartureCityId());
            stat.setString(7, flightSchedule.getStatus());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add flight schedule: %s", flightSchedule));
        }
    }

    @Override
    public void add(List<FlightSchedule> flightSchedules) {
        String sqlQuery = "INSERT INTO flight_schedule (team_request_id, employee_id, start_datetime, end_datetime, destination_city_id, departure_city_id, status) VALUES (? , ?, ?, ?, ?, ?, ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (FlightSchedule flightSchedule : flightSchedules) {
                stat.setInt(1, flightSchedule.getFlightId());
                stat.setInt(2, flightSchedule.getEmployeeId());
                stat.setTimestamp(3, flightSchedule.getStartDateTime());
                stat.setTimestamp(4, flightSchedule.getEndDateTime());
                stat.setInt(5, flightSchedule.getDestinationCityId());
                stat.setInt(6, flightSchedule.getDepartureCityId());
                stat.setString(7, flightSchedule.getStatus());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add flight schedules: %s", flightSchedules));
        }
    }

    @Override
    public FlightSchedule get(int id) {
        FlightSchedule flightSchedule = null;
        String sqlQuery = "SELECT * FROM flight_schedule WHERE flight_schedule.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    flightSchedule = new FlightSchedule()
                            .setId(result.getInt(1))
                            .setFlightId(result.getInt(2))
                            .setEmployeeId(result.getInt(3))
                            .setStartDateTime(result.getTimestamp(4))
                            .setEndDateTime(result.getTimestamp(5))
                            .setDestinationCityId(result.getInt(6))
                            .setDepartureCityId(result.getInt(7))
                            .setStatus(result.getString(8));
            }

        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't get flight schedule by id: %s", id));
        }

        return flightSchedule;
    }

    @Override
    public List<FlightSchedule> getAll() {
        List<FlightSchedule> flightSchedules = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM flight_schedule")) {
                while (result.next()) {
                    FlightSchedule flightSchedule = new FlightSchedule()
                            .setId(result.getInt(1))
                            .setFlightId(result.getInt(2))
                            .setEmployeeId(result.getInt(3))
                            .setStartDateTime(result.getTimestamp(4))
                            .setEndDateTime(result.getTimestamp(5))
                            .setDestinationCityId(result.getInt(6))
                            .setDepartureCityId(result.getInt(7))
                            .setStatus(result.getString(8));
                    flightSchedules.add(flightSchedule);
                }
            }

        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException("Couldn't get all flight schedules");
        }
        return flightSchedules;

    }

    @Override
    public void update(FlightSchedule flightSchedule) {
        String sqlQuery = "UPDATE flight_schedule \n" +
                "SET team_request_id = ?, employee_id = ?, start_datetime=?, end_datetime=?, destination_city_id=?, departure_city_id=?, status = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, flightSchedule.getFlightId());
            stat.setInt(2, flightSchedule.getEmployeeId());
            stat.setTimestamp(3, flightSchedule.getStartDateTime());
            stat.setTimestamp(4, flightSchedule.getEndDateTime());
            stat.setInt(5, flightSchedule.getDestinationCityId());
            stat.setInt(6, flightSchedule.getDepartureCityId());
            stat.setString(7, flightSchedule.getStatus());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't update  flight schedule: %s", flightSchedule));
        }
    }

    @Override
    public void update(List<FlightSchedule> flightSchedules) {
        String sqlQuery = "UPDATE flight_schedule \n" +
                "SET team_request_id = ?, employee_id = ?, start_datetime=?, end_datetime=?, destination_city_id=?, departure_city_id=?, status = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (FlightSchedule flightSchedule : flightSchedules) {
                stat.setInt(1, flightSchedule.getFlightId());
                stat.setInt(2, flightSchedule.getEmployeeId());
                stat.setTimestamp(3, flightSchedule.getStartDateTime());
                stat.setTimestamp(4, flightSchedule.getEndDateTime());
                stat.setInt(5, flightSchedule.getDestinationCityId());
                stat.setInt(6, flightSchedule.getDepartureCityId());
                stat.setString(7, flightSchedule.getStatus());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't update  flight schedules: %s", flightSchedules));
        }
    }

    @Override
    public void delete(int id) {
        String sqlQuery = "DELETE FROM flight_schedule WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't delete  flight schedule by id: %s", id));
        }
    }
}
