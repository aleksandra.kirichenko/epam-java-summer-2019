package com.epam.rd.summer2019.service;

import com.epam.rd.summer2019.service.model.FlightRequestBO;
import com.epam.rd.summer2019.service.model.FlightTeamParamBO;

import java.util.List;

public interface FlightRequestService {
    List<FlightRequestBO> findByAssignee(Integer assigneeId);

    FlightRequestBO findById(int flightId);

    FlightTeamParamBO findTeamParam(int airplaneId);
}
