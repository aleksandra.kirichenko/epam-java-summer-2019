package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.FlightSchedule;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface FlightScheduleRepository {

    void add(FlightSchedule flightSchedule);

    void add(List<FlightSchedule> flightSchedules);

    FlightSchedule get(int id);

    List<FlightSchedule> getAll();

    void update(FlightSchedule flightSchedule);

    void update(List<FlightSchedule> flightSchedules);

    void delete(int id);
}
