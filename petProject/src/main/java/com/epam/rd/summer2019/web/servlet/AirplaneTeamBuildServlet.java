package com.epam.rd.summer2019.web.servlet;

import com.epam.rd.summer2019.service.FlightRequestService;
import com.epam.rd.summer2019.service.FlightScheduleService;
import com.epam.rd.summer2019.service.impl.FlightRequestServiceImpl;
import com.epam.rd.summer2019.service.impl.FlightScheduleServiceImpl;
import com.epam.rd.summer2019.service.model.FlightRequestBO;
import com.epam.rd.summer2019.service.model.FlightTeamParamBO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "airplaneTeamBuildServlet", urlPatterns = "/team/build")
public class AirplaneTeamBuildServlet extends HttpServlet {
    private FlightRequestService flightRequestService = new FlightRequestServiceImpl();
    private FlightScheduleService flightScheduleService = new FlightScheduleServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String[]> scheduleParam = request.getParameterMap();
        flightScheduleService.saveFlightSchedule(scheduleParam);
        request.getRequestDispatcher("/dispatcher").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int flightId = Integer.parseInt(request.getParameter("flightId"));
        FlightRequestBO flightRequestBO = flightRequestService.findById(flightId);
        FlightTeamParamBO teamParam = flightRequestService.findTeamParam(flightRequestBO.getAirplane().getId());
        request.setAttribute("teamParams", teamParam.getTeamParamMaps().entrySet());
        request.setAttribute("flightId", flightId);
        request.getRequestDispatcher("/airplane-team-building-form.jsp").forward(request, response);
    }
}
