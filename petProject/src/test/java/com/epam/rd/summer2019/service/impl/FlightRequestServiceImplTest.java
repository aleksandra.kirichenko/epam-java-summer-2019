package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.repository.*;
import com.epam.rd.summer2019.repository.impl.*;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

public class FlightRequestServiceImplTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    TeamRequestRepository teamRequestRepository = new TeamRequestRepositoryImpl();
    @Mock
    TeamParamRepository teamParamRepository = new TeamParamRepositoryImpl();
    @Mock
    AirlinePositionRepository airlinePositionRepository = new AirlinePositionRepositoryImpl();
    @Mock
    EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    @Mock
    AirplaneRepository airplaneRepository = new AirplaneRepositoryImpl();

    @Test
    public void findByAssignee() {
    }

    @Test
    public void findTeamParam() {
    }

    @Test
    public void findById() {
    }
}