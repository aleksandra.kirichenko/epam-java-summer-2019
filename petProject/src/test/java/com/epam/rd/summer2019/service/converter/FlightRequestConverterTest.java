package com.epam.rd.summer2019.service.converter;

import com.epam.rd.summer2019.repository.AirplaneRepository;
import com.epam.rd.summer2019.repository.CityRepository;
import com.epam.rd.summer2019.repository.EmployeeRepository;
import com.epam.rd.summer2019.repository.model.Airplane;
import com.epam.rd.summer2019.repository.model.City;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.repository.model.TeamRequest;
import com.epam.rd.summer2019.service.model.FlightRequestBO;
import org.hamcrest.Matchers;
import org.hamcrest.junit.MatcherAssert;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

public class FlightRequestConverterTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    AirplaneRepository airplaneRepository;
    @Mock
    CityRepository cityRepository;

    @InjectMocks
    FlightRequestConverter flightRequestConverter = new FlightRequestConverter();

    @Test
    public void convertingToBos() {
        // GIVEN
        Date now = new Date();
        java.sql.Date sqlDate = new java.sql.Date(now.getTime());

        TeamRequest firstDto = new TeamRequest().setId(1).
                setAirplaneId(1).
                setFlightStartDateTime(sqlDate).
                setFlightEndDateTime(sqlDate).
                setDestinationCityId(1).
                setDepartureCityId(2).
                setReporter(1).
                setAssignee(2);

        TeamRequest secondDto = new TeamRequest().setId(2).
                setAirplaneId(2).
                setFlightStartDateTime(sqlDate).
                setFlightEndDateTime(sqlDate).
                setDestinationCityId(3).
                setDepartureCityId(1).
                setReporter(2).
                setAssignee(1);

        TeamRequest thirdDto = new TeamRequest().setId(3).
                setAirplaneId(3).
                setFlightStartDateTime(sqlDate).
                setFlightEndDateTime(sqlDate).
                setDestinationCityId(3).
                setDepartureCityId(2).
                setReporter(4).
                setAssignee(1);
        List<TeamRequest> teamRequestList = Stream.of(firstDto, secondDto, thirdDto).collect(Collectors.toList());

        Employee petrov = new Employee().setId(1).setAirlinePositionId(1).setName("Petrov");
        Employee sidorov = new Employee().setId(2).setAirlinePositionId(1).setName("Sidorov");
        Employee ivanov = new Employee().setId(4).setAirlinePositionId(1).setName("Ivanov");

        doReturn(petrov).when(employeeRepository).get(1);
        doReturn(sidorov).when(employeeRepository).get(2);
        doReturn(ivanov).when(employeeRepository).get(4);

        Airplane airbusA220 = new Airplane().setId(1).setName("Airbus A220").setPassenger(1);
        Airplane boeing717 = new Airplane().setId(2).setName("Boeing 717").setPassenger(1);
        Airplane bristol170 = new Airplane().setId(3).setName("Bristol 170").setPassenger(1);
        doReturn(airbusA220).when(airplaneRepository).get(1);
        doReturn(boeing717).when(airplaneRepository).get(2);
        doReturn(bristol170).when(airplaneRepository).get(3);

        doReturn(new City().setCountryCode("UA").setName("Dnipro").setId(1)).when(cityRepository).get(1);
        doReturn(new City().setCountryCode("UA").setName("Dnipro").setId(2)).when(cityRepository).get(2);
        doReturn(new City().setCountryCode("UA").setName("Dnipro").setId(3)).when(cityRepository).get(3);

        // WHEN
        List<FlightRequestBO> flightRequestBOS = flightRequestConverter.toBos(teamRequestList);

        // THEN
        Assert.assertEquals(teamRequestList.size(), flightRequestBOS.size());

        FlightRequestBO firstBo = new FlightRequestBO().setFlightId(1).setAirplane(airbusA220).setDepartureDateTime(now).setDestinationDateTime(now).setReporter(petrov).setAssignee(sidorov);
        FlightRequestBO secondBo = new FlightRequestBO().setFlightId(2).setAirplane(boeing717).setDepartureDateTime(now).setDestinationDateTime(now).setReporter(sidorov).setAssignee(petrov);
        FlightRequestBO thirdBo = new FlightRequestBO().setFlightId(3).setAirplane(bristol170).setDepartureDateTime(now).setDestinationDateTime(now).setReporter(ivanov).setAssignee(petrov);
        MatcherAssert.assertThat(flightRequestBOS, Matchers.contains(firstBo, secondBo, thirdBo));
    }
}