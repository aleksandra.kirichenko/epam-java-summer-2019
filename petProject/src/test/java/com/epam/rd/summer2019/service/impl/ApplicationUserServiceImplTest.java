package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.repository.AirlinePositionRepository;
import com.epam.rd.summer2019.repository.EmployeeRepository;
import com.epam.rd.summer2019.repository.impl.AirlinePositionRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.EmployeeRepositoryImpl;
import com.epam.rd.summer2019.repository.model.AirlinePosition;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.service.model.ApplicationUser;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

public class ApplicationUserServiceImplTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    @Mock
    AirlinePositionRepository airlinePositionRepository = new AirlinePositionRepositoryImpl();

    @InjectMocks
    ApplicationUserServiceImpl applicationUserService = new ApplicationUserServiceImpl();

    @Test
    public void getApplicationUserByName() {
        // GIVEN
        Employee vilisov = new Employee().setName("vilisov").setAirlinePositionId(2).setId(2);
        doReturn(vilisov).when(employeeRepository).getByLogin("vilisov");

        AirlinePosition dispatcher = new AirlinePosition().setId(1).setName("dispatcher");
        doReturn(dispatcher).when(airlinePositionRepository).get(vilisov.getId());

        // WHEN
        ApplicationUser applicationUser = applicationUserService.getApplicationUserByName("vilisov");

        // THEN
        Assert.assertEquals(applicationUser.getUserName(), "vilisov");
        Assert.assertEquals(applicationUser.getRole(), "dispatcher");
        Assert.assertEquals(applicationUser.getPassword(), "123");
    }

    @Test
    public void getByNameAndPasswordIfPresent() {
        // GIVEN
        Employee vilisov = new Employee().setName("vilisov").setAirlinePositionId(2).setId(2);
        doReturn(vilisov).when(employeeRepository).getByLogin("vilisov");

        AirlinePosition dispatcher = new AirlinePosition().setId(1).setName("dispatcher");
        doReturn(dispatcher).when(airlinePositionRepository).get(vilisov.getId());

        // WHEN
        ApplicationUser applicationUser = applicationUserService.getByNamePassword("vilisov", "123");

        // THEN
        Assert.assertEquals(applicationUser.getUserName(), "vilisov");
        Assert.assertEquals(applicationUser.getRole(), "dispatcher");
        Assert.assertEquals(applicationUser.getPassword(), "123");
    }

    @Test
    public void getByNameAndPasswordIfNotPresent() {
        // GIVEN
        Employee vilisov = new Employee().setName("vilisov").setAirlinePositionId(2).setId(2);
        doReturn(vilisov).when(employeeRepository).getByLogin("vilisov");

        AirlinePosition dispatcher = new AirlinePosition().setId(1).setName("dispatcher");
        doReturn(dispatcher).when(airlinePositionRepository).get(vilisov.getId());

        // WHEN
        ApplicationUser applicationUser = applicationUserService.getByNamePassword("vilisov", "wrong_password");

        // THEN
        Assert.assertNull(applicationUser);
    }
}