# homework_2

## This is example of using maven + maven modules

  Создать Java приложение калькулятор которое включает следующий функционал:
	1. Проект собирается с помощью maven
	2. Приложение должно быть запускаемым jar файлом
	3. Приложение должно писать от 10 логов с приоритетами от info до error
(Предполагается, что при запуске приложения, в лог записывается информация что приложение стартовало, выводит версию, имя и т. п. Когда завершается - информацию о завершении выполнения. Не должно быть одного лога в одном методе на все приложение)
	4. Необходимо использовать logback + slf4j
	5. Все логи должны лежать в папке /logs относительно jar файла
6. Разбить логи по файлам:
Логи с приоритетом info - пишутся только в консоль
Логи с приоритетом warn - пишутся только в файл с названием warning.log
Логи с приоритетом error - пишутся только в файл с названием error.log
	7. Ограничить максимальный размер лог файлов 1 МБ.
	
	
При запуске программы, через аргументы командной строки(String[] args) будут передаваться следующие параметры:
number1 number2 operator

Примеры операндов:
100 -100 +

Программа должна вывести в консоль следующее сообщение и завершиться:
number1=100 number2=-100 operator=+ result=0

public interface Calc {
    double addition(double a, double b);

    double subtraction(double a, double b);

    double multiplication(double a, double b);

    double division(double a, double b);
}

Структура проекта:


|   pom.xml
|   README.md
|
\---+---calc-console
    |   |   pom.xml
    |   |   README.md
    |   |
    |   \---src
    |       +---main
    |           \---java
    |               \---com
    |                   \---epam
    |                       \---rd
    |                           \---june2018
    |                               \---calc
	|                                   \---console
	|									    \---App.java
    +---calc-core
    |   |   pom.xml
    |   |   README.md
    |   |
    |   \---src
    |       +---main
    |           \---java
    |               \---com
    |                   \---epam
    |                       \---rd
    |                           \---spring2019
    |                               \---calc
    |                                   \---core
    |									    \---CalcImpl.java
    +---calc-interface
       |   pom.xml
       |   README.md
       |
       \---src
           +---main
               \---java
                   \---com
                       \---epam
                           \---rd
                               \---spring2019
                                    \---calc
	                                    \---interfaces
										    \---Calc.java