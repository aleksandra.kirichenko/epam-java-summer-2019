package com.epam.rd.summer2019.calc.core;

import com.epam.rd.summer2019.calc.interfaces.Calc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CalcImpl implements Calc {
    private static Logger logger = LoggerFactory.getLogger(CalcImpl.class.getName());

    @Override
    public double addition(double a, double b) {
        logger.info("addition");
        return a + b;
    }

    @Override
    public double subtraction(double a, double b) {
        logger.info("subtraction");
        return a - b;
    }

    @Override
    public double multiplication(double a, double b) {
        logger.info("multiplication");
        return a * b;
    }

    @Override
    public double division(double a, double b) {
        logger.info("division");
        if (b != 0) {
            return a / b;
        } else {
            String message = "Division by zero is impossible";
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }
}
