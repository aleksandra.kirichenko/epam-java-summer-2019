package com.epam.rd.summer2019.calc.console;

import com.epam.rd.summer2019.calc.core.CalcImpl;
import com.epam.rd.summer2019.calc.interfaces.Calc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class.getName());

    private static final String ENV_VAR_NUMBER_1 = "number1";
    private static final String ENV_VAR_NUMBER_2 = "number2";
    private static final String OPERATOR = "operator";

    public static void main(String[] args) {
        logger.info("Calculation started \n" + "version: 1.0-SNAPSHOT \n" + "name: calc");

        double number1 = parseEnvDouble(ENV_VAR_NUMBER_1);
        double number2 = parseEnvDouble(ENV_VAR_NUMBER_2);
        String operator = parseEnvString(OPERATOR);

        logger.info(String.format("Calculating: %s %s %s", number1, operator, number2));

        double result;

        Calc calc = new CalcImpl();
        if (number2 == 0 && operator.equals("/"))
            logger.warn("You are trying to make division by zero");
        switch (operator) {
            case "+":
                result = calc.addition(number1, number2);
                break;
            case "-":
                result = calc.subtraction(number1, number2);
                break;
            case "*":
                result = calc.multiplication(number1, number2);
                break;
            case "/":
                result = calc.division(number1, number2);
                break;
            default:
                String message = String.format("Unknown operator: %s", operator);
                logger.error(message);
                throw new IllegalArgumentException(message);
        }
        logger.info(String.format("number1 = %s, number2 = %s, operator = %s, RESULT = %s", number1, number2, operator, result));
        logger.info("Calculation successfully completed");
    }

    private static double parseEnvDouble(String envName) {
        String envValue = System.getenv(envName);
        if (envValue == null || envValue.isEmpty()) {
            logger.error(String.format("Environment variable %s is absent or empty", envName));
            throw new IllegalArgumentException(String.format("Environment variable %s is absent or empty", envName));
        }
        try {
            return Double.parseDouble(envValue);
        } catch (NumberFormatException e) {
            logger.error(String.format("%s not valid double format", envValue), e.getMessage());
            throw e;
        }
    }

    private static String parseEnvString(String envName) {
        String envValue = System.getenv(envName);
        if (envValue == null || envValue.isEmpty()) {
            logger.error(String.format("%s is absent or empty", envName));
            throw new IllegalArgumentException(String.format("%s is absent", envName));
        }
        return envValue;
    }
}
