package com.epam.rd.summer2019.model;

public class City {
    private Integer id;
    private String name;
    private String state;
    private long population;

    public Integer getId() {
        return id;
    }

    public City setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getState() {
        return state;
    }

    public City setState(String state) {
        this.state = state;
        return this;
    }

    public String getName() {
        return name;
    }

    public City setName(String name) {
        this.name = name;
        return this;
    }

    public long getPopulation() {
        return population;
    }

    public City setPopulation(long population) {
        this.population = population;
        return this;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", population=" + population +
                '}';
    }
}
