package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.model.User;
import com.epam.rd.summer2019.repository.UserRepository;
import com.epam.rd.summer2019.utils.ConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    public void add(User user) throws SQLException, IOException {
        String sqlQuery = "INSERT INTO user (first_name, last_name, age, city_id) VALUES (?, ?, ?, ?)";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, user.getFirstName());
            stat.setString(2, user.getLastName());
            stat.setInt(3, user.getAge());
            stat.setInt(4, user.getCityId());
            stat.execute();
        }
    }

    public void add(List<User> users) throws SQLException, IOException {
        String sqlQuery = "INSERT INTO user (first_name, last_name, age, city_id) VALUES (? , ?, ?, ?)";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (User user: users){
                stat.setString(1, user.getFirstName());
                stat.setString(2, user.getLastName());
                stat.setInt(3, user.getAge());
                stat.setInt(4, user.getCityId());
                stat.addBatch();
            }
            stat.executeBatch();
        }
    }

    public User get(int id) throws SQLException, IOException {
        User user = null;
        String sqlQuery = "SELECT * FROM user WHERE user.id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    user = new User().setId(result.getInt(1))
                            .setFirstName(result.getString(2))
                            .setLastName(result.getString(3))
                            .setAge(result.getInt(4))
                            .setCityId(result.getInt(5));
            }

        }

        return user;
    }

    public List<User> getAll() throws SQLException, IOException {
        List<User> users = new ArrayList<>();
        try (Connection connection = ConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM user ")) {
                while (result.next()) {
                    User user = new User()
                            .setId(result.getInt(1))
                            .setFirstName(result.getString(2))
                            .setLastName(result.getString(3))
                            .setAge(result.getInt(4))
                            .setCityId(result.getInt(5));
                    users.add(user);
                }
            }

            return users;
        }
    }

    public void update(User user) throws SQLException, IOException {
        String sqlQuery = "UPDATE user SET first_name = ?, last_name = ?, age = ?, city_id = ? WHERE id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, user.getFirstName());
            stat.setString(2, user.getLastName());
            stat.setInt(3, user.getAge());
            stat.setInt(4, user.getCityId());
            stat.setInt(5, user.getId());
            stat.execute();
        }
    }

    public void update(List<User> users) throws SQLException, IOException {
        String sqlQuery = "UPDATE user SET first_name = ?, last_name = ?, age = ?, city_id = ? WHERE id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (User user: users){
                stat.setString(1, user.getFirstName());
                stat.setString(2, user.getLastName());
                stat.setInt(3, user.getAge());
                stat.setInt(4, user.getCityId());
                stat.setInt(5, user.getId());
                stat.addBatch();
            }stat.executeBatch();
        }
    }

    public void delete(int id) throws SQLException, IOException {
        String sqlQuery = "DELETE FROM user WHERE id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();

        }
    }
}