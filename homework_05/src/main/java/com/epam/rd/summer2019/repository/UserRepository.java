package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.model.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface UserRepository {

    void add(User user) throws SQLException, IOException;

    void add(List<User> users) throws SQLException, IOException;

    User get(int id) throws SQLException, IOException;

    List<User> getAll() throws SQLException, IOException;

    void update(User user) throws SQLException, IOException;

    void update(List<User> users) throws SQLException, IOException;

    void delete(int id) throws SQLException, IOException;
}
