package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.model.City;
import com.epam.rd.summer2019.repository.CityRepository;
import com.epam.rd.summer2019.utils.ConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CityRepositoryImpl implements CityRepository {

    @Override
    public void add(City city) throws SQLException, IOException {
        String sqlQuery = "INSERT INTO city (name, state, population) VALUES (? , ?, ?)";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, city.getName());
            stat.setString(2, city.getState());
            stat.setLong(3, city.getPopulation());
            stat.execute();
        }
    }

    @Override
    public void add(List<City> cities) throws SQLException, IOException {
        String sqlQuery = "INSERT INTO city (name, state, population) VALUES (? , ?, ?)";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (City city : cities) {
                stat.setString(1, city.getName());
                stat.setString(2, city.getState());
                stat.setLong(3, city.getPopulation());
                stat.addBatch();
            }
            stat.executeBatch();
        }
    }

    @Override
    public City get(int id) throws SQLException, IOException {
        City city = null;
        String sqlQuery = "SELECT * FROM city WHERE city.id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    city = new City()
                            .setId(result.getInt(1))
                            .setName(result.getString(2))
                            .setState(result.getString(3))
                            .setPopulation(result.getLong(4));
            }

        }

        return city;
    }

    @Override
    public List<City> getAll() throws SQLException, IOException {
        List<City> cities = new ArrayList<>();
        try (Connection connection = ConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM city")) {
                while (result.next()) {
                    City city = new City()
                            .setId(result.getInt(1))
                            .setName(result.getString(2))
                            .setState(result.getString(3))
                            .setPopulation(result.getLong(4));
                    cities.add(city);
                }
            }

            return cities;
        }
    }

    @Override
    public void update(City city) throws SQLException, IOException {
        String sqlQuery = "UPDATE city \n" +
                "SET name = ?, state = ?, population = ? WHERE id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, city.getName());
            stat.setString(2, city.getState());
            stat.setLong(3, city.getPopulation());
            stat.setInt(4, city.getId());
            stat.execute();
        }

    }

    @Override
    public void update(List<City> cities) throws SQLException, IOException {
        String sqlQuery = "UPDATE city \n" +
                "SET name = ?, state = ?, population = ? WHERE id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (City city : cities) {
                stat.setString(1, city.getName());
                stat.setString(2, city.getState());
                stat.setLong(3, city.getPopulation());
                stat.setInt(4, city.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        }
    }

    @Override
    public void delete(int id) throws SQLException, IOException {
        String sqlQuery = "DELETE FROM city WHERE id = ?";
        try (Connection connection = ConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        }
    }
}