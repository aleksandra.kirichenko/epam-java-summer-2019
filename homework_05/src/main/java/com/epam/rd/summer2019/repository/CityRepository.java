package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.model.City;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CityRepository {

    void add(City city) throws SQLException, IOException;

    void add(List<City> cities) throws SQLException, IOException;

    City get(int id) throws SQLException, IOException;

    List<City> getAll() throws SQLException, IOException;

    void update(City city) throws SQLException, IOException;

    void update(List<City> city) throws SQLException, IOException;

    void delete(int id) throws SQLException, IOException;
}

