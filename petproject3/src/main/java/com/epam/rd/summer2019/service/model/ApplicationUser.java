package com.epam.rd.summer2019.service.model;

public class ApplicationUser {
    String userName;
    String password;
    String role;

    public ApplicationUser() {
        password = "123";
    }

    public String getUserName() {
        return userName;
    }

    public ApplicationUser setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public ApplicationUser setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getRole() {
        return role;
    }

    public ApplicationUser setRole(String role) {
        this.role = role;
        return this;
    }

    @Override
    public String toString() {
        return "ApplicationUser{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
