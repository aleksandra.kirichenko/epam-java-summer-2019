package com.epam.rd.summer2019.repository.model;

public class AirlinePosition {
    private Integer id;
    private String name;

    public int getId() {
        return id;
    }

    public AirlinePosition setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AirlinePosition setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "AirlinePosition{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
