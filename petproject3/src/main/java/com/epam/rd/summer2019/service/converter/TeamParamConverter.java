package com.epam.rd.summer2019.service.converter;

import com.epam.rd.summer2019.repository.model.TeamParam;
import com.epam.rd.summer2019.service.model.FlightTeamParamBO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeamParamConverter {
    public FlightTeamParamBO toFlightTeamParam(List<TeamParam> teamParams, Map<Integer, String> positionTable) {
        Map<String, Integer> param = new HashMap<>();
        for (TeamParam teamParam : teamParams) {
            String position = positionTable.get(teamParam.getPosition());
            if (teamParam.getMinNumber() > 0) {
                param.put(position, teamParam.getMinNumber());
            }
        }
        return new FlightTeamParamBO().setTeamParamMaps(param);
    }
}
