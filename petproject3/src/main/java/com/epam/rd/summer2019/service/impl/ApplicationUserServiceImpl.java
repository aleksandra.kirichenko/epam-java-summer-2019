package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.repository.AirlinePositionRepository;
import com.epam.rd.summer2019.repository.EmployeeRepository;
import com.epam.rd.summer2019.repository.impl.AirlinePositionRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.EmployeeRepositoryImpl;
import com.epam.rd.summer2019.repository.model.AirlinePosition;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.service.ApplicationUserService;
import com.epam.rd.summer2019.service.converter.ApplicationUserConverter;
import com.epam.rd.summer2019.service.model.ApplicationUser;

public class ApplicationUserServiceImpl implements ApplicationUserService {

    EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    AirlinePositionRepository airlinePositionRepository = new AirlinePositionRepositoryImpl();
    ApplicationUserConverter applicationUserConverter = new ApplicationUserConverter();

    @Override
    public ApplicationUser getApplicationUserByName(String name) {
        Employee employee = employeeRepository.getByLogin(name);
        if (employee == null) {
            return null;
        }
        AirlinePosition position = airlinePositionRepository.get(employee.getAirlinePositionId());
        return applicationUserConverter.toApplicationUser(employee, position);
    }

    @Override
    public boolean permissionDenied(ApplicationUser user, String requestURI) {
        return false;
    }

    @Override
    public ApplicationUser getByNamePassword(String name, String password) {
        ApplicationUser user = getApplicationUserByName(name);
        if (user == null) {
            return null;
        }
        return user.getPassword().equals(password) ? user : null;
    }
}
