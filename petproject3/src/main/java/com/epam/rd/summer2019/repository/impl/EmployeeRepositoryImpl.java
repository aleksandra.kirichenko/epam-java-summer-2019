package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.EmployeeRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeRepositoryImpl implements EmployeeRepository {
    @Override
    public void add(Employee employee) {
        String sqlQuery = "INSERT INTO employee (airline_position_id, name) VALUES (? , ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, employee.getAirlinePositionId());
            stat.setString(2, employee.getName());
            stat.execute();
        } catch (SQLException | IOException e) {
            //TODO create log message
            throw new AirlineSqlException(String.format("Couldn't add employee: %s", employee), e);
        }
    }

    @Override
    public void add(List<Employee> employees) {
        String sqlQuery = "INSERT INTO employee (airline_position_id, name) VALUES (? ,  ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (Employee employee:employees) {
                stat.setInt(1, employee.getAirlinePositionId());
                stat.setString(2, employee.getName());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            //TODO create log message
            throw new AirlineSqlException(String.format("Couldn't add employees: %s", employees), e);
        }

    }

    @Override
    public Employee get(int id) {
        Employee employee = null;
        String sqlQuery = "SELECT * FROM employee WHERE employee.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    employee = new Employee()
                            .setId(result.getInt(1))
                            .setAirlinePositionId(result.getInt(2))
                            .setName(result.getString(3));
            }

        } catch (SQLException | IOException e) {
            //TODO create log message
            throw new AirlineSqlException(String.format("Couldn't get employee by id: %s", id), e);
        }

        return employee;
    }

    @Override
    public Employee getByLogin(String name) {
        String sqlQuery = "SELECT * FROM employee WHERE employee.name = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, name);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    return new Employee()
                            .setId(result.getInt(1))
                            .setAirlinePositionId(result.getInt(2))
                            .setName(result.getString(3));
            }

        } catch (SQLException | IOException e) {
            //TODO create log message
            throw new AirlineSqlException(String.format("Couldn't get employee by name: %s", name), e);
        }

        return null;
    }

    @Override
    public List<Employee> getAll() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM employee")) {
                while (result.next()) {
                    Employee employee = new Employee()
                            .setId(result.getInt(1))
                            .setAirlinePositionId(result.getInt(2))
                            .setName(result.getString(3));
                    employees.add(employee);
                }
            }

        } catch (SQLException | IOException e) {
            throw new AirlineSqlException("Couldn't get all employees", e);
        }
        return employees;
    }

    @Override
    public void update(Employee employee) {
        String sqlQuery = "UPDATE employee SET airline_position_id=?, name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, employee.getAirlinePositionId());
            stat.setString(2, employee.getName());
            stat.setInt(3, employee.getId());
            stat.execute();
        } catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't update employee: %s", employee), e);
        }

    }

    @Override
    public void update(List<Employee> employees) {
        String sqlQuery = "UPDATE employee SET airline_position_id=?, name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (Employee employee:employees) {
                stat.setInt(1, employee.getAirlinePositionId());
                stat.setString(2, employee.getName());
                stat.setInt(3, employee.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't update employees: %s", employees), e);

        }
    }

    @Override
    public void delete(int id){
        String sqlQuery = "DELETE FROM city WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        }catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't delete employees by id: %s", id), e);
        }   }
}
