package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.TeamRequestRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.TeamRequest;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;
import org.mariadb.jdbc.internal.logging.Logger;
import org.mariadb.jdbc.internal.logging.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamRequestRepositoryImpl implements TeamRequestRepository {

    private static Logger logger = LoggerFactory.getLogger(TeamRequestRepositoryImpl.class);

    @Override
    public void add(TeamRequest teamRequest) {
        String sqlQuery = "INSERT INTO team_request (flight_start_datetime, flight_end_datetime, destination_city_id, departure_city_id) VALUES (? , ?, ?, ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            fillStatement(teamRequest, stat, false);
            stat.execute();
        } catch (SQLException | IOException e) {
            logger.error("Couldn't add team request : %s", teamRequest);
            throw new AirlineSqlException(String.format("Couldn't add team request : %s", teamRequest));
        }
    }

    @Override
    public void add(List<TeamRequest> teamRequests) {
        String sqlQuery = "INSERT INTO team_request (flight_start_datetime, flight_end_datetime, destination_city_id, departure_city_id) VALUES (? , ?, ?, ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (TeamRequest teamRequest : teamRequests) {
                fillStatement(teamRequest, stat, false);
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            logger.error("Couldn't add team requests : %s", teamRequests);
            throw new AirlineSqlException(String.format("Couldn't add team requests : %s", teamRequests));
        }
    }

    @Override
    public TeamRequest get(int id) {
        TeamRequest teamRequest = null;
        String sqlQuery = "SELECT * FROM team_request WHERE team_request.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    teamRequest = resultToTeamRequest(result);
            }

        } catch (SQLException | IOException e) {
            logger.error(String.format("Couldn't get team request by id : %s", id));
            throw new AirlineSqlException(String.format("Couldn't get team request by id : %s", id));
        }

        return teamRequest;
    }


    @Override
    public List<TeamRequest> getAll() {
        List<TeamRequest> teamRequests = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM team_request")) {
                while (result.next()) {
                    teamRequests.add(resultToTeamRequest(result));
                }
            }

        } catch (SQLException | IOException e) {
            logger.error("Couldn't get all team request");
            throw new AirlineSqlException("Couldn't get all team request");
        }
        return teamRequests;

    }

    @Override
    public void update(TeamRequest teamRequest) {
        String sqlQuery = "UPDATE team_request \n" +
                "SET airplane_id=?, flight_start_datetime=?, flight_end_datetime=?, destination_city_id=?, departure_city_id=? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            fillStatement(teamRequest, stat, true);
            stat.execute();
        } catch (SQLException | IOException e) {
            logger.error(String.format("Couldn't update team request : %s", teamRequest));
            throw new AirlineSqlException(String.format("Couldn't update team request : %s", teamRequest));
        }
    }

    @Override
    public void update(List<TeamRequest> teamRequests) {
        String sqlQuery = "UPDATE team_request \n" +
                "SET airplane_id=?, flight_start_datetime=?, flight_end_datetime=?, destination_city_id=?, departure_city_id=? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (TeamRequest teamRequest : teamRequests) {
                fillStatement(teamRequest, stat, true);
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            logger.error(String.format("Couldn't update team requests : %s", teamRequests));
            throw new AirlineSqlException(String.format("Couldn't update team requests : %s", teamRequests));
        }
    }

    @Override
    public void delete(int id) {
        String sqlQuery = "DELETE FROM team_request WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        } catch (SQLException | IOException e) {
            logger.error(String.format("Couldn't delete team request by id : %s", id));
            throw new AirlineSqlException(String.format("Couldn't delete team request by id : %s", id));
        }
    }

    @Override
    public List<TeamRequest> getByAssigneeId(int id) {
        List<TeamRequest> teamRequests = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement("SELECT * FROM team_request WHERE assignee_id = ?")) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                while (result.next()) {
                    TeamRequest teamRequest = resultToTeamRequest(result);
                    teamRequests.add(teamRequest);
                }
            }
        } catch (SQLException | IOException e) {
            logger.error(String.format("Couldn't get assignee  by id : %s", id));
            throw new AirlineSqlException(String.format("Couldn't get assignee  by id : %s", id));
        }
        return teamRequests;

    }

    private TeamRequest resultToTeamRequest(ResultSet result) throws SQLException {
        return new TeamRequest()
                .setId(result.getInt(1))
                .setAirplaneId(result.getInt(2))
                .setFlightStartDateTime(result.getDate(3))
                .setFlightEndDateTime(result.getDate(4))
                .setDestinationCityId(result.getInt(5))
                .setDepartureCityId(result.getInt(6))
                .setReporter(result.getInt(7))
                .setAssignee(result.getInt(8));
    }

    private void fillStatement(TeamRequest teamRequest, PreparedStatement stat, boolean forUpdate) throws SQLException {
        stat.setInt(1, teamRequest.getAirplaneId());
        stat.setDate(2, teamRequest.getFlightStartDateTime());
        stat.setDate(3, teamRequest.getFlightStartDateTime());
        stat.setInt(4, teamRequest.getDestinationCityId());
        stat.setInt(5, teamRequest.getDepartureCityId());
        if (forUpdate) {
            stat.setInt(6, teamRequest.getId());
        }
    }
}
