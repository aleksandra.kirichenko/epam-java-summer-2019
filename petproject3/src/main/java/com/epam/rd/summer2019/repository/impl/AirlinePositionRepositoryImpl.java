package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.AirlinePositionRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.AirlinePosition;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AirlinePositionRepositoryImpl implements AirlinePositionRepository {
    @Override
    public void add(AirlinePosition airlinePosition) {
        String sqlQuery = "INSERT INTO airline_position VALUES (? )";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, airlinePosition.getName());
            stat.execute();
        } catch (SQLException | IOException e) {
            //TODO create log message
            throw new AirlineSqlException(String.format("Couldn't add airline position: %s", airlinePosition), e);
        }

    }

    @Override
    public void add(List<AirlinePosition> airlinePositions) {
        String sqlQuery = "INSERT INTO airline_position VALUES (?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (AirlinePosition airlinePosition : airlinePositions) {
                stat.setString(1, airlinePosition.getName());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            //TODO create log message
            throw new AirlineSqlException(String.format("Couldn't add airline positions: %s", airlinePositions), e);
        }

    }

    @Override
    public AirlinePosition get(int id) {
        AirlinePosition airlinePosition = null;
        String sqlQuery = "SELECT * FROM airline_position WHERE airline_position.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    airlinePosition = new AirlinePosition()
                            .setId(result.getInt(1))
                            .setName(result.getString(2));
            }

        } catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't get airline position by id: %s", id), e);
        }
        return airlinePosition;
    }

    @Override
    public List<AirlinePosition> getAll() {
        List<AirlinePosition> airlinePositions = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM airline_position")) {
                while (result.next()) {
                    AirlinePosition airlinePosition = new AirlinePosition()
                            .setId(result.getInt(1))
                            .setName(result.getString(2));
                    airlinePositions.add(airlinePosition);
                }
            }

        } catch (SQLException | IOException e) {
            throw new AirlineSqlException("Couldn't get all airline positions");
        }
        return airlinePositions;

    }

    @Override
    public void update(AirlinePosition airlinePosition) {
        String sqlQuery = "UPDATE airline_position \n" +
                "SET name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, airlinePosition.getName());
            stat.setInt(2, airlinePosition.getId());
            stat.execute();
        } catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't update airline position: %s", airlinePosition), e);
        }
    }

    @Override
    public void update(List<AirlinePosition> airlinePositions) {
        String sqlQuery = "UPDATE airline_position \n" +
                "SET name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (AirlinePosition airlinePosition : airlinePositions) {
                stat.setString(1, airlinePosition.getName());
                stat.setInt(2, airlinePosition.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't update airline positions: %s", airlinePositions), e);
        }

    }

    @Override
    public void delete(int id) {
        String sqlQuery = "DELETE FROM airline_position  WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        } catch (SQLException | IOException e) {
            throw new AirlineSqlException(String.format("Couldn't delete airline positionby id: %s", id), e);
        }
    }
}