package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.TeamParam;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface TeamParamRepository {
    void add(TeamParam teamParam);

    void add(List<TeamParam> teamParams);

    TeamParam get(int id);

    List<TeamParam> getAll();

    void update(TeamParam teamParam);

    void update(List<TeamParam> teamParams);

    void delete(int id);

    List<TeamParam> getByAirplaneId(int airplaneId);
}
