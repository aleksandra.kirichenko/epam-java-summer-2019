package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.AirplaneRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.Airplane;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AirplaneRepositoryImpl implements AirplaneRepository {
    @Override
    public void add(Airplane airplane) {
        String sqlQuery = "INSERT INTO airplane (name, passenger) VALUES (? , ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, airplane.getName());
            stat.setInt(2, airplane.getPassenger());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add Airplane: %s", airplane));
        }

    }

    @Override
    public void add(List<Airplane> airplanes) {
        String sqlQuery = "INSERT INTO airplane (name,passenger) VALUES (?,?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (Airplane airplane : airplanes) {
                stat.setString(1, airplane.getName());
                stat.setInt(2, airplane.getPassenger());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add Airplanes: %s", airplanes));
        }
    }

    @Override
    public Airplane get(int id) {
        Airplane airplane = null;
        String sqlQuery = "SELECT * FROM airplane WHERE airplane.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    airplane = new Airplane()
                            .setId(result.getInt(1))
                            .setName(result.getString(2))
                            .setPassenger(result.getInt(3));
            }

        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't fetch Airplane by id: %s", id));
        }
        return airplane;
    }

    @Override
    public List<Airplane> getAll() {
        List<Airplane> airplanes = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM airplane")) {
                while (result.next()) {
                    Airplane airplane = new Airplane()
                            .setId(result.getInt(1))
                            .setName(result.getString(2))
                            .setPassenger(result.getInt(4));
                    airplanes.add(airplane);
                }
            }

        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException("Couldn't get all Airplanes");
        } return airplanes;

    }

    @Override
    public void update(Airplane airplane) {
        // TODO fix this
        String sqlQuery = "UPDATE airplane SET name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, airplane.getName());
            stat.setInt(3, airplane.getPassenger());
            stat.setInt(4, airplane.getId());
            stat.execute();
        }catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't update Airplane: %s", airplane));
        }

    }

    @Override
    public void update(List<Airplane> airplanes) {
        // TODO fix this
        String sqlQuery = "UPDATE airplane \n" +
                "SET name = ? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (Airplane airplane : airplanes) {
                stat.setString(1, airplane.getName());
                stat.setInt(3, airplane.getPassenger());
                stat.setInt(4, airplane.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        }catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't update Airplanes: %s", airplanes));
        }
    }

    @Override
    public void delete(int id) {
        String sqlQuery = "DELETE FROM airplane  WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        }catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't delete Airplane by id: %s",id));
        }
    }
}
