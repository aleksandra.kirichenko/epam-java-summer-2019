package com.epam.rd.summer2019.repository.model;

public class Employee {
    private Integer id;
    private int airlinePositionId;
    private String name;

    public int getId() {
        return id;
    }

    public Employee setId(Integer id) {
        this.id = id;
        return this;
    }

    public int getAirlinePositionId() {
        return airlinePositionId;
    }

    public Employee setAirlinePositionId(int airlinePositionId) {
        this.airlinePositionId = airlinePositionId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", airlinePosition=" + airlinePositionId +
                ", name='" + name + '\'' +
                '}';
    }
}
