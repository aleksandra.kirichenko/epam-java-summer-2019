package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.TeamRequest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface TeamRequestRepository {
    void add(TeamRequest teamRequest);

    void add(List<TeamRequest> teamRequests);

    TeamRequest get(int id);

    List<TeamRequest> getAll();

    void update(TeamRequest teamRequest);

    void update(List<TeamRequest> teamRequests);

    void delete(int id);

    List<TeamRequest> getByAssigneeId(int id);
}
