package com.epam.rd.summer2019.web.model;

public class FlightRequestResponse {
    private int flightId;
    private String airplaneName;
    private String departureDateTime;
    private String destinationDateTime;
    private String reporter;
    private String assignee;
    private String airplaneTeamBuildUrl;

    public int getFlightId() {
        return flightId;
    }

    public FlightRequestResponse setFlightId(int flightId) {
        this.flightId = flightId;
        return this;
    }

    public String getAirplaneName() {
        return airplaneName;
    }

    public FlightRequestResponse setAirplaneName(String airplaneName) {
        this.airplaneName = airplaneName;
        return this;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public FlightRequestResponse setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
        return this;
    }

    public String getDestinationDateTime() {
        return destinationDateTime;
    }

    public FlightRequestResponse setDestinationDateTime(String destinationDateTime) {
        this.destinationDateTime = destinationDateTime;
        return this;
    }

    public String getReporter() {
        return reporter;
    }

    public FlightRequestResponse setReporter(String reporter) {
        this.reporter = reporter;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public FlightRequestResponse setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public String getAirplaneTeamBuildUrl() {
        return airplaneTeamBuildUrl;
    }

    public FlightRequestResponse setAirplaneTeamBuildUrl(String airplaneTeamBuildUrl) {
        this.airplaneTeamBuildUrl = airplaneTeamBuildUrl;
        return this;
    }
}
