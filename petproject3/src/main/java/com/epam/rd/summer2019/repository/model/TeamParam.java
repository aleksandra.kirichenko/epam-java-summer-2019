package com.epam.rd.summer2019.repository.model;

public class TeamParam {
    private Integer id;
    private int airlineModelId;
    private int position;
    private int minNumber;

    public Integer getId() {
        return id;
    }

    public TeamParam setId(Integer id) {
        this.id = id;
        return this;
    }

    public int getAirlineModelId() {
        return airlineModelId;
    }

    public TeamParam setAirlineModelId(int airlineModelId) {
        this.airlineModelId = airlineModelId;
        return this;
    }

    public int getPosition() {
        return position;
    }

    public TeamParam setPosition(int position) {
        this.position = position;
        return this;
    }

    public int getMinNumber() {
        return minNumber;
    }

    public TeamParam setMinNumber(int minNumber) {
        this.minNumber = minNumber;
        return this;
    }

    @Override
    public String toString() {
        return "TeamParam{" +
                "id=" + id +
                ", airlineModelId=" + airlineModelId +
                ", position=" + position +
                ", minNumber=" + minNumber +
                '}';
    }
}
