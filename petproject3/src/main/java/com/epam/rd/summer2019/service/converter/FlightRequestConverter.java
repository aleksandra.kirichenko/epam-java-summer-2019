package com.epam.rd.summer2019.service.converter;

import com.epam.rd.summer2019.repository.AirplaneRepository;
import com.epam.rd.summer2019.repository.CityRepository;
import com.epam.rd.summer2019.repository.EmployeeRepository;
import com.epam.rd.summer2019.repository.impl.AirplaneRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.CityRepositoryImpl;
import com.epam.rd.summer2019.repository.impl.EmployeeRepositoryImpl;
import com.epam.rd.summer2019.repository.model.Airplane;
import com.epam.rd.summer2019.repository.model.City;
import com.epam.rd.summer2019.repository.model.Employee;
import com.epam.rd.summer2019.repository.model.TeamRequest;
import com.epam.rd.summer2019.service.model.FlightRequestBO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FlightRequestConverter {
    EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    AirplaneRepository airplaneRepository = new AirplaneRepositoryImpl();
    CityRepository cityRepository = new CityRepositoryImpl();

    public List<FlightRequestBO> toBos(List<TeamRequest> teamRequests) {

        Map<Integer, Airplane> airplaneTableByTeamRequest = teamRequests.stream()
                .map(TeamRequest::getAirplaneId)
                .distinct()
                .map(id -> airplaneRepository.get(id))
                .collect(Collectors.toMap(Airplane::getId, Function.identity()));

        Map<Integer, Employee> reporterTableByTeamRequest = teamRequests.stream()
                .map(TeamRequest::getReporter)
                .distinct()
                .map(id -> employeeRepository.get(id))
                .collect(Collectors.toMap(Employee::getId, Function.identity()));

        Map<Integer, Employee> assigneeTableByTeamRequest = teamRequests.stream()
                .map(TeamRequest::getAssignee)
                .distinct()
                .map(id -> employeeRepository.get(id))
                .collect(Collectors.toMap(Employee::getId, Function.identity()));

        Map<Integer, City> departureCityTableByTeamRequest = teamRequests.stream()
                .map(TeamRequest::getDepartureCityId)
                .distinct()
                .map(id -> cityRepository.get(id))
                .collect(Collectors.toMap(City::getId, Function.identity()));

        Map<Integer, City> destinationCityTableByTeamRequest = teamRequests.stream()
                .map(TeamRequest::getDestinationCityId)
                .distinct()
                .map(id -> cityRepository.get(id))
                .collect(Collectors.toMap(City::getId, Function.identity()));


        List<FlightRequestBO> ret = new ArrayList<>();
        teamRequests.forEach(teamRequest -> {
            FlightRequestBO flightRequestBO = flightRequestBO(teamRequest,
                    reporterTableByTeamRequest.get(teamRequest.getReporter()),
                    assigneeTableByTeamRequest.get(teamRequest.getAssignee()),
                    airplaneTableByTeamRequest.get(teamRequest.getAirplaneId()),
                    departureCityTableByTeamRequest.get(teamRequest.getDepartureCityId()),
                    destinationCityTableByTeamRequest.get(teamRequest.getDestinationCityId())
            );
            ret.add(flightRequestBO);
        });
        return ret;
    }

    private FlightRequestBO flightRequestBO(TeamRequest teamRequest, Employee reporter, Employee assignee, Airplane airplane, City departure, City destination) {
        FlightRequestBO flightRequestBO = new FlightRequestBO();
        flightRequestBO.setFlightId(teamRequest.getId());
        flightRequestBO.setAirplane(airplane);
        flightRequestBO.setDepartureDateTime(teamRequest.getFlightStartDateTime());
        flightRequestBO.setDestinationDateTime(teamRequest.getFlightEndDateTime());
        flightRequestBO.setDepartureCity(departure);
        flightRequestBO.setDestinationCity(destination);
        flightRequestBO.setReporter(reporter);
        flightRequestBO.setAssignee(assignee);
        return flightRequestBO;
    }
}
