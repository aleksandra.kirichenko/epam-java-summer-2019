package com.epam.rd.summer2019.repository.impl;

import com.epam.rd.summer2019.repository.TeamParamRepository;
import com.epam.rd.summer2019.repository.exception.AirlineSqlException;
import com.epam.rd.summer2019.repository.model.TeamParam;
import com.epam.rd.summer2019.repository.utils.MariaDbConnectorUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamParamRepositoryImpl implements TeamParamRepository {
    @Override
    public void add(TeamParam teamParam) {
        String sqlQuery = "INSERT INTO team_param (airline_model_id, position, min_number) VALUES (? , ?, ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, teamParam.getAirlineModelId());
            stat.setInt(2, teamParam.getPosition());
            stat.setInt(3, teamParam.getMinNumber());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add team parameters : %s", teamParam));
        }

    }

    @Override
    public void add(List<TeamParam> teamParams){
        String sqlQuery = "INSERT INTO team_param (airline_model_id, position, min_number) VALUES (? , ?, ?)";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (TeamParam teamParam : teamParams) {
                stat.setInt(1, teamParam.getAirlineModelId());
                stat.setInt(2, teamParam.getPosition());
                stat.setInt(3, teamParam.getMinNumber());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add team parameters : %s", teamParams));
        }

    }

    @Override
    public TeamParam get(int id){
        TeamParam teamParam = null;
        String sqlQuery = "SELECT * FROM team_param WHERE team_param.id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    teamParam = new TeamParam()
                            .setId(result.getInt(1))
                            .setAirlineModelId(result.getInt(2))
                            .setPosition(result.getInt(3))
                            .setMinNumber(result.getInt(4));
            }

        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't delete team parameters by id: %s", id));
        }
        return teamParam;
    }

    @Override
    public List<TeamParam> getAll() {
        List<TeamParam> teamParams = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM team_param")) {
                while (result.next()) {
                    TeamParam teamParam = new TeamParam()
                            .setId(result.getInt(1))
                            .setAirlineModelId(result.getInt(2))
                            .setPosition(result.getInt(3))
                            .setMinNumber(result.getInt(4));
                    teamParams.add(teamParam);
                }
            }

        } catch (SQLException | IOException e) {
            throw new AirlineSqlException("Couldn't get all team parameters : %s");
        }
        return teamParams;

    }

    @Override
    public void update(TeamParam teamParam) {
        String sqlQuery = "UPDATE team_param \n" +
                "SET airline_model_id=?, position=?, min_number=? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, teamParam.getAirlineModelId());
            stat.setInt(2, teamParam.getPosition());
            stat.setInt(3, teamParam.getMinNumber());
            stat.setInt(4, teamParam.getId());
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't add team parameters : %s", teamParam));
        }
    }

    @Override
    public void update(List<TeamParam> teamParams) {
        String sqlQuery = "UPDATE team_param \n" +
                "SET airline_model_id=?, position=?, min_number=? WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (TeamParam teamParam : teamParams) {
                stat.setInt(1, teamParam.getAirlineModelId());
                stat.setInt(2, teamParam.getPosition());
                stat.setInt(3, teamParam.getMinNumber());
                stat.setInt(4, teamParam.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't update team parameters : %s", teamParams));
        }
    }

    @Override
    public void delete(int id) {
        String sqlQuery = "DELETE FROM team_param WHERE id = ?";
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        } catch (SQLException | IOException e) {
            // TODO log error message
            throw new AirlineSqlException(String.format("Couldn't delete team parameters by id: %s", id));
        }
    }

    @Override
    public List<TeamParam> getByAirplaneId(int airplaneId) {

        List<TeamParam> teamParams = new ArrayList<>();
        try (Connection connection = MariaDbConnectorUtils.getConnection(); PreparedStatement stat = connection.prepareStatement("SELECT * FROM team_param WHERE airplane_id = ?")) {
            stat.setInt(1, airplaneId);
            try (ResultSet result = stat.executeQuery()) {
                while (result.next()) {
                    TeamParam teamParam = new TeamParam()
                            .setId(result.getInt(1))
                            .setAirlineModelId(result.getInt(2))
                            .setPosition(result.getInt(3))
                            .setMinNumber(result.getInt(4));
                    teamParams.add(teamParam);
                }
            }

        } catch (SQLException | IOException e) {
            throw new AirlineSqlException("Couldn't get all team parameters : %s");
        }
        return teamParams;
    }
}
