package com.epam.rd.summer2019.web.servlet;

import com.epam.rd.summer2019.service.FlightRequestService;
import com.epam.rd.summer2019.service.impl.FlightRequestServiceImpl;
import com.epam.rd.summer2019.service.model.FlightRequestBO;
import com.epam.rd.summer2019.web.converter.FlightRequestResponseConverter;
import com.epam.rd.summer2019.web.model.FlightRequestResponse;
import com.epam.rd.summer2019.web.utils.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "airlineDispatcherServlet", urlPatterns = "/dispatcher")
public class AirlineDispatcherServlet extends HttpServlet {

    private FlightRequestService flightRequestService = new FlightRequestServiceImpl();
    private FlightRequestResponseConverter converter = new FlightRequestResponseConverter();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int currentLoggedInUserId = ServletUtils.getCurrentLoggedInUser();
        List<FlightRequestBO> flightRequestBOS = flightRequestService.findByAssignee(currentLoggedInUserId);
        List<FlightRequestResponse> flightRequestResponses = converter.toResponses(flightRequestBOS, request);
        request.setAttribute("flightRequestResponses", flightRequestResponses);
        request.getRequestDispatcher("airline-dispatcher.jsp").forward(request, response);
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
