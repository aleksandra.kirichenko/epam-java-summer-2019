package com.epam.rd.summer2019.repository.model;
import java.sql.*;

public class TeamRequest {
    private Integer id;
    private Integer airplaneId;
    private Date flightStartDateTime;
    private Date flightEndDateTime;
    private Integer destinationCityId;
    private Integer departureCityId;
    private Integer reporter;
    private Integer assignee;

    public Integer getId() {
        return id;
    }

    public TeamRequest setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getAirplaneId() {
        return airplaneId;
    }

    public TeamRequest setAirplaneId(Integer airplaneId) {
        this.airplaneId = airplaneId;
        return this;
    }

    public Date getFlightStartDateTime() {
        return flightStartDateTime;
    }

    public TeamRequest setFlightStartDateTime(Date flightStartDateTime) {
        this.flightStartDateTime = flightStartDateTime;
        return this;
    }

    public Date getFlightEndDateTime() {
        return flightEndDateTime;
    }

    public TeamRequest setFlightEndDateTime(Date flightEndDateTime) {
        this.flightEndDateTime = flightEndDateTime;
        return this;
    }

    public Integer getDestinationCityId() {
        return destinationCityId;
    }

    public TeamRequest setDestinationCityId(Integer destinationCityId) {
        this.destinationCityId = destinationCityId;
        return this;
    }

    public Integer getDepartureCityId() {
        return departureCityId;
    }

    public TeamRequest setDepartureCityId(Integer departureCityId) {
        this.departureCityId = departureCityId;
        return this;
    }

    public Integer getReporter() {
        return reporter;
    }

    public TeamRequest setReporter(Integer reporter) {
        this.reporter = reporter;
        return this;
    }

    public Integer getAssignee() {
        return assignee;
    }

    public TeamRequest setAssignee(Integer assignee) {
        this.assignee = assignee;
        return this;
    }
}
