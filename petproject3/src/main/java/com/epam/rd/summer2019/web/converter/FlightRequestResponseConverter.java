package com.epam.rd.summer2019.web.converter;

import com.epam.rd.summer2019.service.model.FlightRequestBO;
import com.epam.rd.summer2019.web.model.FlightRequestResponse;
import com.epam.rd.summer2019.web.utils.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FlightRequestResponseConverter {

    private final String DATETIME_UI_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public List<FlightRequestResponse> toResponses(List<FlightRequestBO> flightRequestBOList, HttpServletRequest request) {
        return flightRequestBOList.stream()
                .filter(Objects::nonNull)
                .map(bo -> toResponse(bo, request))
                .collect(Collectors.toList());
    }

    private FlightRequestResponse toResponse(FlightRequestBO flightRequestBO, HttpServletRequest httpRequest) {
        return new FlightRequestResponse()
                .setFlightId(flightRequestBO.getFlightId())
                .setAirplaneName(flightRequestBO.getAirplane().getName())
                .setDepartureDateTime(formatDate(flightRequestBO.getDepartureDateTime()))
                .setDestinationDateTime(formatDate(flightRequestBO.getDestinationDateTime()))
                .setReporter(flightRequestBO.getAssignee().getName())
                .setAssignee(flightRequestBO.getAssignee().getName())
                .setAirplaneTeamBuildUrl(buildUrl(httpRequest, flightRequestBO.getFlightId()));
    }

    private String buildUrl(HttpServletRequest httpRequest, int flightId) {
        return String.format("%s/team/build?flightId=%s", ServletUtils.getBaseUrl(httpRequest), flightId);
    }

    private String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_UI_FORMAT);
        return simpleDateFormat.format(date);
    }
}
