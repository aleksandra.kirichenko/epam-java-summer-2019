package com.epam.rd.summer2019.service.impl;

import com.epam.rd.summer2019.repository.FlightScheduleRepository;
import com.epam.rd.summer2019.repository.impl.FlightScheduleRepositoryImpl;
import com.epam.rd.summer2019.repository.model.FlightSchedule;
import com.epam.rd.summer2019.service.FlightScheduleService;
import com.epam.rd.summer2019.service.converter.FlightRequestConverter;
import com.epam.rd.summer2019.service.converter.FlightScheduleConverter;

import java.util.List;
import java.util.Map;

public class FlightScheduleServiceImpl implements FlightScheduleService {
    private FlightScheduleRepository flightScheduleRepository = new FlightScheduleRepositoryImpl();
    private FlightScheduleConverter flightScheduleConverter = new FlightScheduleConverter();
    @Override
    public void saveFlightSchedule(Map<String, String[]> scheduleParam) {
        List<FlightSchedule> flightSchedules = flightScheduleConverter.convert(scheduleParam);
        flightScheduleRepository.add(flightSchedules);
    }

    @Override
    public void getRequested() {

    }
}
