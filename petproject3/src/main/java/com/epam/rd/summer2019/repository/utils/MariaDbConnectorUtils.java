package com.epam.rd.summer2019.repository.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MariaDbConnectorUtils {

    public static Connection getConnection() throws SQLException, IOException {

        Properties props = new Properties();
        try (InputStream in =
                     MariaDbConnectorUtils.class.getClassLoader().getResourceAsStream("db.properties")) {
            props.load(in);
            String drivers = props.getProperty("driver");
            if (drivers != null) {
                System.setProperty("driver", drivers);
                Class.forName(drivers);
            }
        } catch (ClassNotFoundException e) {
            throw new SQLException("Couldn't load driver for MariaDB", e);
        }

        String url = props.getProperty("url");
        String username = props.getProperty("username");
        String password = props.getProperty("password");

        return DriverManager.getConnection(url, username, password);
    }


}
