package com.epam.rd.summer2019.repository.exception;

public class AirlineSqlException extends RuntimeException {
    public AirlineSqlException() {
        super();
    }

    public AirlineSqlException(String message) {
        super(message);
    }

    public AirlineSqlException(String message, Throwable cause) {
        super(message, cause);
    }

    public AirlineSqlException(Throwable cause) {
        super(cause);
    }

    protected AirlineSqlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
