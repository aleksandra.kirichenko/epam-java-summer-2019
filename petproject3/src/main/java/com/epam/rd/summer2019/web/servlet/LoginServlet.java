package com.epam.rd.summer2019.web.servlet;

import com.epam.rd.summer2019.service.ApplicationUserService;
import com.epam.rd.summer2019.service.impl.ApplicationUserServiceImpl;
import com.epam.rd.summer2019.service.model.ApplicationUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "loginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private final ApplicationUserService userService = new ApplicationUserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("login");
        String password = request.getParameter("password");
        ApplicationUser user = userService.getByNamePassword(name, password);
        if (user == null) {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }else{
            HttpSession session = request.getSession(true);
            session.setAttribute("user", user);
            request.getRequestDispatcher(getUriByUser(user)).forward(request, response);
        }
    }

    private String getUriByUser(ApplicationUser user) {
        switch (user.getRole()) {
            case "dispatcher":
                return "/dispatcher";
            case "administrator":
                return "/admin";
            default: return "/";
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
}
