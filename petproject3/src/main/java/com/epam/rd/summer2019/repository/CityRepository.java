package com.epam.rd.summer2019.repository;

import com.epam.rd.summer2019.repository.model.City;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CityRepository {

    void add(City city);

    void add(List<City> cities);

    City get(int id);

    List<City> getAll();

    void update(City city);

    void update(List<City> city);

    void delete(int id);
}
