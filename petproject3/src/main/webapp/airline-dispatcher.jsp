<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">

    <title>Dispatcher</title>
</head>
<body>
<div class="container">
    <c:set var="flightRequestResponses" scope="session" value="${param.flightRequestResponses}"/>
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h1>Team requests</h1>
        </div>
    </div>
    <c:forEach var="flightRequestResponse" items="${flightRequestResponses}">
        <div class="row">
            <div class="col-1"><c:out value='${flightRequestResponse.flightId}'/></div>
            <div class="col-2"><c:out value='${flightRequestResponse.airplaneName}'/></div>
            <div class="col"><c:out value='${flightRequestResponse.departureDateTime}'/></div>
            <div class="col"><c:out value='${flightRequestResponse.destinationDateTime}'/></div>
            <div class="col-1"><c:out value='${flightRequestResponse.reporter}'/></div>
            <div class="col"><c:out value='${flightRequestResponse.destinationDateTime}'/></div>
            <div class="col"><a href="<c:out value='${flightRequestResponse.airplaneTeamBuildUrl}'/>">build team</a>
            </div>
        </div>
    </c:forEach>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>

</body>
</html>
