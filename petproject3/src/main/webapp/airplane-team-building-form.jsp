<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/css/bootstrap.min.css">

    <title>Dispatcher</title>
</head>

<body>
<div class="container">
    `
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h1>Team building form</h1>
        </div>
    </div>
    <c:set var="teamParams" scope="session" value="${param.teamParams}"/>
    <c:set var="flightId" scope="session" value="${param.flightId}"/>
    <div class="row">
        <form class="col-6" action="${pageContext.request.contextPath}/team/build" method="post">
            <c:forEach var="teamParam" items="${teamParams}">
                <c:forEach var="i" begin="1" end="${teamParam.value+1}">
                    <div class="form-group">
                        <label for="${teamParam.key}_${i}">
                            <c:out value="${teamParam.key} ${i}"/>
                        </label>
                        <input type="text" name="${teamParam.key}" class="form-control"
                               id="${teamParam.key}_${i}"/>
                    </div>
                </c:forEach>
            </c:forEach>
            <input type="hidden" name="flightId" value="${flightId}">
            <button type="submit" class="btn btn-primary">Create team</button>
        </form>
    </div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/libs/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/libs/propper-1.11.0/popper.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/libs/bootstrap-4.1.3/js/bootstrap.min.js"></script>

</body>
</html>


