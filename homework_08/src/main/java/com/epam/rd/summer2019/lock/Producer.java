package com.epam.rd.summer2019.lock;

class Producer extends Thread {
    private MyQueue queue;

    public Producer(MyQueue queue) {
        this.queue = queue;
        this.setName("Producer");
    }

    @Override
    public void run() {
        for (int i = 1; i <=5 ; i++) {
            queue.put(i);
        }
    }
}
