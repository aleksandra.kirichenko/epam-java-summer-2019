package com.epam.rd.summer2019.lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class MyQueue {
    private int value;
    private boolean valueSet = false;
    final ReentrantLock lock;
    final Condition condition;

    private static Logger logger = LoggerFactory.getLogger(MyQueue.class.getName());

    public MyQueue() {
        lock = new ReentrantLock();
        condition = lock.newCondition();
    }

    public int get() {
        lock.lock();
        try {
            while (!valueSet)
                condition.await();
            logger.info("Received: " + value);
            valueSet = false;
            condition.signalAll();
            return value;
        } catch (InterruptedException e) {
            logger.info("Get interruption was handled");
            Thread.currentThread().interrupt();
            return 0;
        } finally {
            lock.unlock();
        }
    }

    public void put(int value) {
        lock.lock();
        try {
            while (valueSet)
                condition.await();
            this.value = value;
            valueSet = true;
            logger.info("Sent: " + value);
            condition.signalAll();
        } catch (InterruptedException e) {
            logger.info("Put interruption was handled");
            return;
        } finally {
            lock.unlock();

        }

    }
}
