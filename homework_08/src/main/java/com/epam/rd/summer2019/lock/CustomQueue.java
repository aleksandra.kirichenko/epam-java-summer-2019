package com.epam.rd.summer2019.lock;

public class CustomQueue {

    public static void main(String[] args) throws InterruptedException {
        MyQueue queue  = new MyQueue();
        Consumer consumer = new Consumer(queue);
        Producer producer = new Producer(queue);
        producer.start();
        consumer.start();
        Thread.sleep(500);
        consumer.interrupt();
    }

}


