package com.epam.rd.summer2019.singleton;

public class TreadSafeSingleton {
    private static TreadSafeSingleton instance;

    TreadSafeSingleton() {
    }

    public static synchronized TreadSafeSingleton getInstance() {
        if (instance == null) {
            instance = new TreadSafeSingleton();
        }
        return instance;
    }
}
