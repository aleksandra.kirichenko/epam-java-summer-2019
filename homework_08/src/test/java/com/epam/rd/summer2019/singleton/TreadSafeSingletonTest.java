package com.epam.rd.summer2019.singleton;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TreadSafeSingletonTest {

    @Test
    public void assertSingleton() {
        Set<TreadSafeSingleton> setOfSingletons = Stream.generate(TreadSafeSingleton::getInstance).limit(1000).collect(Collectors.toSet());
        Assert.assertEquals("Size of singeltons sets can not be more than 1", 1, setOfSingletons.size());
    }
}