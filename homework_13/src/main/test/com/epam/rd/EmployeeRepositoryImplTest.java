package com.epam.rd;

import com.epam.rd.model.Employee;
import com.epam.rd.repository.impl.EmployeeRepositoryImpl;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;

public class EmployeeRepositoryImplTest {

    EmployeeRepositoryImpl employeeRepository = new EmployeeRepositoryImpl();

    @Test
    public void add() throws IOException, SQLException {
        employeeRepository.add(new Employee()
                .setPosition("pilot")
                .setSurname("Petrov")
        );
        getAl1();
    }

    @Test
    public void get() throws SQLException, IOException {
        System.out.println(employeeRepository.get(3));
    }

    @org.junit.Test
    public void getAl1() throws IOException, SQLException {
        employeeRepository.getAll().forEach(System.out::println);
    }

    @Test
    public void update() throws IOException, SQLException {
        employeeRepository.update(new Employee()
                .setPosition("pilot")
                .setSurname("Nanфффф")
                .setId(1));
        getAl1();
    }

    @Test
    public void update1() throws IOException, SQLException {
        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(new Employee()
                .setPosition("pilot")
                .setSurname("Vencev")
                .setId(2));
        employees.add(new Employee()
                .setPosition("pilot")
                .setSurname("Levchenko")
                .setId(3));
        employeeRepository.update(employees);
        getAl1();
    }

    @Test
    public void delete() throws IOException, SQLException {
        employeeRepository
                .delete(1);
        getAl1();
    }
}
