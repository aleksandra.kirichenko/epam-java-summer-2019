package com.epam.rd.validator;

import com.epam.rd.model.Employee;

import java.util.List;

public interface EmployeeValidator {
    void validateEmployee(Employee employee);

    void validateEmployeesExist(List<Employee> employees);
}
