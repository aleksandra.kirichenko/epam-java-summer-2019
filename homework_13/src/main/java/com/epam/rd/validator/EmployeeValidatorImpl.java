package com.epam.rd.validator;

import com.epam.rd.exeption.ValidationException;
import com.epam.rd.model.Employee;
import com.epam.rd.repository.EmployeeRepository;
import com.epam.rd.repository.impl.EmployeeRepositoryImpl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class EmployeeValidatorImpl implements EmployeeValidator {
    private final static String REGEX_PATTERN = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$";

    private EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();

    @Override
    public void validateEmployee(Employee employee){
        Pattern p = Pattern.compile(REGEX_PATTERN);
        Matcher m = p.matcher(employee.getSurname());
        if (!m.matches()) {
            throw new ValidationException("Invalid user name: " + employee.getSurname());
        }



    }

    @Override
    public void validateEmployeesExist(List<Employee> employees) {
        final List<Integer> ids = employees.stream().map(Employee::getId).collect(Collectors.toList());
        final List<Integer> foundEmployeesIds = employeeRepository.getByIds(ids).stream().map(Employee::getId).collect(Collectors.toList());
        ids.removeAll(foundEmployeesIds);
        if(ids.size()>0){
            throw new ValidationException(String.format("Couldn't update employees with ids: %s",
                    ids.stream().map(String::valueOf).collect(Collectors.joining(", "))));
        }
    }
}
