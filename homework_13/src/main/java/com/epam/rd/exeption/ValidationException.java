package com.epam.rd.exeption;

public class ValidationException extends ApplicationException {
    public ValidationException(String message) {
        super(message);
    }
}
