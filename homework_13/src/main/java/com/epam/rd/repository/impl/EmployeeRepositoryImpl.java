package com.epam.rd.repository.impl;

import com.epam.rd.model.Employee;
import com.epam.rd.repository.EmployeeRepository;
import com.epam.rd.utils.ConnectorUtils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static com.epam.rd.utils.ConnectorUtils.getConnection;

public class EmployeeRepositoryImpl implements EmployeeRepository {
    @Override
    public void add(Employee employee) throws SQLException, IOException {
        String sqlQuery = "INSERT INTO employee (position, surname) VALUES (?, ?)";
        try (Connection connection = getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, employee.getPosition());
            stat.setString(2, employee.getSurname());
            stat.execute();
        }
    }

    @Override
    public Employee get(int id) throws SQLException, IOException {
        Employee employee = null;
        String sqlQuery = "SELECT * FROM employee WHERE employee.id = ?";
        try (Connection connection = getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    employee = new Employee()
                            .setId(result.getInt(1))
                            .setPosition(result.getString(2))
                            .setSurname(result.getString(3));
            }
        }
        return employee;
    }

    public List<Employee> getAll() throws SQLException, IOException {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = getConnection(); Statement stat = connection.createStatement()) {
            try (ResultSet result = stat.executeQuery("SELECT * FROM employee ")) {
                while (result.next()) {
                    Employee employee = new Employee()
                            .setId(result.getInt(1))
                            .setPosition(result.getString(2))
                            .setSurname(result.getString(3));
                    employees.add(employee);
                }
            }

            return employees;
        }
    }

    @Override
    public void update(Employee employee) throws SQLException, IOException {
        String sqlQuery = "UPDATE employee SET position = ?, surname = ? WHERE id = ?";
        try (Connection connection = getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setString(1, employee.getPosition());
            stat.setString(2, employee.getSurname());
            stat.setInt(3, employee.getId());
            stat.execute();
        }
    }

    @Override
    public void update(List<Employee> employees) throws SQLException, IOException {
        String sqlQuery = "UPDATE employee SET position = ?, surname = ? WHERE id = ?";
        try (Connection connection = getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            for (Employee employee : employees) {
                stat.setString(1, employee.getPosition());
                stat.setString(2, employee.getSurname());
                stat.setInt(3, employee.getId());
                stat.addBatch();
            }
            stat.executeBatch();
        }
    }

    @Override
    public void delete(int id) throws SQLException, IOException {
        String sqlQuery = "DELETE FROM employee WHERE id = ?";
        try (Connection connection = getConnection(); PreparedStatement stat = connection.prepareStatement(sqlQuery)) {
            stat.setInt(1, id);
            stat.execute();
        }

    }


}
