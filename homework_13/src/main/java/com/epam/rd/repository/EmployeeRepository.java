package com.epam.rd.repository;

import com.epam.rd.model.Employee;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface EmployeeRepository {
    Employee get(int id) throws SQLException, IOException;

    List<Employee> getAll() throws SQLException, IOException;

    List<Employee> getByIds(List<Integer> ids);

    void add(Employee employee) throws SQLException, IOException;

    void update(Employee employee) throws SQLException, IOException;

    void update(List<Employee> employees) throws SQLException, IOException;

    void delete(int id) throws SQLException, IOException;
}
