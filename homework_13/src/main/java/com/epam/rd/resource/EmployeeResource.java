package com.epam.rd.resource;

import com.epam.rd.exeption.ValidationException;
import com.epam.rd.model.Employee;
import com.epam.rd.repository.EmployeeRepository;
import com.epam.rd.repository.impl.EmployeeRepositoryImpl;
import com.epam.rd.validator.EmployeeValidator;
import com.epam.rd.validator.EmployeeValidatorImpl;
import org.mariadb.jdbc.internal.logging.Logger;
import org.mariadb.jdbc.internal.logging.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Path(value="/employee")
public class EmployeeResource {
    private static Logger logger = LoggerFactory.getLogger(EmployeeResource.class);


    EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    EmployeeValidator validator = new EmployeeValidatorImpl();

    @HEAD
    @Produces("application/json")
    public Response getName(){
        return Response.ok().build();
    }

    @GET
    @Produces("application/json")
    @Path("/all")
    public Response getEmployees(){
        try {
            return Response.ok(employeeRepository.getAll()).build();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @GET
    @Produces("application/json")
    public Response getEmployee(@QueryParam("id") int id){
        try {
            final Employee employee = employeeRepository.get(id);
            return employee == null ? Response.status(BAD_REQUEST).build() : Response.ok().build();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/update")
    @Produces("application/json")
    public Response updateEmployee(Employee employee){
        try {
            employeeRepository.update(employee);
            return Response.ok().build();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/update/all")
    @Produces("application/json")
    public Response updateEmployee(List<Employee> employees){
        try {
            validator.validateEmployeesExist(employees);
            if(employeeRepository.== null){
                return Response.status(BAD_REQUEST).build();
            }
            employeeRepository.update(employees);
            return Response.ok().build();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @DELETE
    @Produces("application/json")
    public Response deleteEmployee(@QueryParam("id") int id){
        try {
            if(employeeRepository.get(id)== null){
             return Response.status(BAD_REQUEST).build();
            }
            employeeRepository.delete(id);
            return Response.ok().build();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @POST
    @Consumes("application/json")
    public Response addEmployee(Employee employee) {
        try {
            validator.validateEmployee(employee);
            employeeRepository.add(employee);
            return Response.ok().build();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }catch (ValidationException e){
            e.printStackTrace();
            return Response.status(BAD_REQUEST).build();
        }
    }

}