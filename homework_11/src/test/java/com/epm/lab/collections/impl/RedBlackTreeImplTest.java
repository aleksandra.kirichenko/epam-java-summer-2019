package com.epm.lab.collections.impl;

import com.epm.lab.collections.Map;
import org.junit.Test;

import static org.junit.Assert.*;

public class RedBlackTreeImplTest {

    @Test
    public void get() {
        // GIVEN
        Map<String, Integer> map = new RedBlackTreeImpl<>();
        map.put("first", 1);
        map.put("second", 2);
        map.put("third", 3);

        // WHEN
        Integer actualFirst = map.get("first");
        Integer actualSecond = map.get("second");
        Integer actualThird= map.get("third");

        // THEN
        assertEquals("First node added anr retrieved", new Integer(1), actualFirst);
        assertEquals("Second node added anr retrieved", new Integer(2), actualSecond);
        assertEquals("Third node added anr retrieved", new Integer(3), actualThird);
        assertEquals("Total map size should be equal 3", 3, map.size());
    }

}