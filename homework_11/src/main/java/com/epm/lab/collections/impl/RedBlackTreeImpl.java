package com.epm.lab.collections.impl;

import com.epm.lab.collections.Map;
import com.epm.lab.collections.OrderedMap;

import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

public class RedBlackTreeImpl<Key extends Comparable<Key>, Value> implements OrderedMap<Key, Value> {
    private static final boolean RED = Boolean.TRUE;
    private static final boolean BLACK = Boolean.FALSE;
    private Entry root;

    private class Entry {
        private Key key;
        private Value value;
        private Entry left;
        private Entry right;
        private boolean color;
        private int size;

        public Entry(Key key, Value value, boolean color, int size) {
            this.key = key;
            this.value = value;
            this.color = color;
            this.size = size;
        }
    }

    @Override
    public Value get(Key key) {
        return Optional.ofNullable(key)
                .map(notEmptyKey -> get(root, key))
                .orElseThrow(() -> new IllegalArgumentException("Key can not be empty"));
    }

    private Value get(Entry entry, Key key) {
        while (entry != null) {
            int compareResult = key.compareTo(entry.key);
            if (compareResult < 0) {
                entry = entry.left;
            } else if (compareResult > 0) {
                entry = entry.right;
            } else {
                return entry.value;
            }
        }
        return null;
    }

    @Override
    public void put(Key key, Value value) {
        Optional.ofNullable(key).orElseThrow(() -> new IllegalArgumentException("Key can not be empty"));
        //TODO check on NULL value. In case value == null - delete key from tree
        root = put(root, key, value);
        root.color = BLACK;
    }

    @Override
    public Iterator<Map.Entry<Key, Value>> iterator() {
        // TODO implement
        return null;
    }

    private Entry put(Entry entry, Key key, Value value) {
        if (entry == null) {
            return new Entry(key, value, RED, 1);
        }
        int compareResult = key.compareTo(entry.key);
        if (compareResult < 0) {
            entry.left = put(entry.left, key, value);
        } else if (compareResult > 0) {
            entry.right = put(entry.right, key, value);
        } else {
            entry.value = value;
        }

        if (isRed(entry.right) && !isRed(entry.left)) {
            entry = rotateLeft(entry);
        }
        if (isRed(entry.left) && isRed(entry.left.left)) {
            entry = rotateRight(entry);
        }
        if (isRed(entry.left) && isRed(entry.right)) {
            flipColors(entry);
        }
        entry.size = size(entry.left) + size(entry.right) + 1;
        return entry;
    }

    private Entry rotateRight(Entry entry) {
        Entry x = entry.left;
        entry.left = x.right;
        x.right = entry;
        x.color = x.right.color;
        x.right.color = RED;
        x.size = entry.size;
        entry.size = size(entry.left) + size(entry.right) + 1;
        return x;
    }

    private Entry rotateLeft(Entry entry) {
        Entry x = entry.right;
        entry.right = x.left;
        x.left = entry;
        x.color = x.left.color;
        x.left.color = RED;
        x.size = entry.size;
        entry.size = size(entry.left) + size(entry.right) + 1;
        return x;
    }

    private void flipColors(Entry entry) {
        entry.color = !entry.color;
        entry.left.color = !entry.left.color;
        entry.right.color = !entry.right.color;
    }

    private boolean isRed(Entry x) {
        return Optional.ofNullable(x).map(notEmptyEntry -> notEmptyEntry.color == RED).orElse(false);
    }

    private int size(Entry x) {
        return Optional.ofNullable(x).map(notEmptyEntry -> notEmptyEntry.size).orElse(0);
    }

    public int size() {
        return size(root);
    }

    public boolean isEmpty() {
        return Objects.isNull(root);
    }

}